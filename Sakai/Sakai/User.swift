//
//  User.swift
//  Sakai
//
//  Created by Thien Lan on 11/24/16.
//  Copyright © 2016 Team 4. All rights reserved.
//

import Foundation
class User{
    var username:String?
    var token:String?
    var email:String?
    var password:String
    
    init(username:String, password:String, email:String, token:String ) {
        self.username = username
        self.password = password
        self.email = email
        self.token = token
    }
}
class GetUser{
    static var getUser:User?
}
