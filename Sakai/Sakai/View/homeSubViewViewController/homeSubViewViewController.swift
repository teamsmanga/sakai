//
//  homeSubViewViewController.swift
//  Sakai
//
//  Created by Developer on 11/30/16.
//  Copyright © 2016 Team 4. All rights reserved.
//

import UIKit

class homeSubViewViewController: UIViewController {
    //*----------------------------------
    var numberOfRowsEachSection = Int()
    var numberOfSections = Int()
    var arrChapterOffline:[Chapter]? = nil
    var isHidden = true
    var dic_idLesson_arrChapter = [String:[Chapter]]()
    var arrIdLessons = [String]()
    //*----------------------------------
    
    @IBOutlet weak var myTableView: UITableView!
    //*----------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        //
        myTableView.rowHeight = UITableViewAutomaticDimension
        myTableView.estimatedRowHeight = 200
        myTableView.tableFooterView = UIView()
        //
        myTableView.register(UINib(nibName:"homeSubView_row", bundle: nil), forCellReuseIdentifier: "homeSubView_row")
        //
    }
    
    // thực hiện push toi view showAll khi chạm header cua section
    func tapHeaderSection(sender: UITapGestureRecognizer) {
        let showAllView = ShowAllTableViewController()
        let idLesson = arrIdLessons[(sender.view?.tag)!]
        showAllView.idLesson = idLesson
        showAllView.title = Lesson.getLessonByIdLesson(idLesson).lesson_name
        self.navigationController?.pushViewController(showAllView, animated: true)
    }
    
    fileprivate func getData() {
        arrIdLessons = Lesson.getIdLessonOffline()
        for item in arrIdLessons {
            dic_idLesson_arrChapter[item] = Chapter.getAllChaptersOfflineByIdLesson(item)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getData()
        if arrIdLessons.count > 0 {
            myTableView.reloadData()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension homeSubViewViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrIdLessons.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Chapter.getAllChaptersOfflineByIdLesson(arrIdLessons[section]).count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellId = "homeSubView_row"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
        //
        let strIdLesson = arrIdLessons[indexPath.section]
        let arrChapter = dic_idLesson_arrChapter[strIdLesson]
        let chapter = arrChapter?[indexPath.row]
        cell.textLabel?.text = chapter?.chapter_name
        //
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let titleLesson = Lesson.getLessonByIdLesson(arrIdLessons[section]).lesson_name
        return titleLesson
    }
}

extension homeSubViewViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let story = UIStoryboard(name: "Main", bundle: nil)
//        let detailView = story.instantiateViewController(withIdentifier: "detailView") as! DetailViewController
        let strIdLesson = arrIdLessons[indexPath.section]
//        let arrChapter = dic_idLesson_arrChapter[strIdLesson]
//        detailView.idChapter = arrChapter?[indexPath.row].chapter_id
//        self.navigationController?.pushViewController(detailView, animated: true)
        
        let story = UIStoryboard(name: "Main", bundle: nil)
        let lessonVC = story.instantiateViewController(withIdentifier: "lessonViewController") as? LessonViewController
        let arrChapterOffline = Chapter.getAllChaptersOfflineByIdLesson(strIdLesson)
        let idChapter = arrChapterOffline[indexPath.row].chapter_id
        lessonVC?.idChapter = idChapter
        self.navigationController?.pushViewController(lessonVC!, animated: true)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        // tao view cho header section
        let view = UIView(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(tableView.frame.size.width), height: CGFloat(18)))
        let label = UILabel(frame: CGRect(x: CGFloat(10), y: CGFloat(5), width: CGFloat(tableView.frame.size.width), height: CGFloat(18)))
        label.font = UIFont.boldSystemFont(ofSize: CGFloat(17))
        let titleLesson = Lesson.getLessonByIdLesson(arrIdLessons[section]).lesson_name
        label.text = titleLesson
        view.addSubview(label)
        view.backgroundColor = UIColor(red: CGFloat(166 / 255.0), green: CGFloat(177 / 255.0), blue: CGFloat(186 / 255.0), alpha: CGFloat(1.0))
        // add gesture cho view header section
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapHeaderSection))
        tapGesture.numberOfTouchesRequired = 1;
        tapGesture.numberOfTapsRequired = 1;
        view.addGestureRecognizer(tapGesture)
        view.tag = section
        //
        return view
    }
    
    @objc(tableView:commitEditingStyle:forRowAtIndexPath:) func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        let arrchapter = Chapter.getAllChaptersOfflineByIdLesson(arrIdLessons[indexPath.section]) // get all chapter by idlesson
        let currChapter = arrchapter[indexPath.row] // get current chapter at index

        // remove chapter from table ChapterOffline
        Chapter.removeOfflineChapter(currChapter)// remove chapter in table chapter offline by object curChapter
      
        let myVideo = Video.getVideoByIdChapter(currChapter.chapter_id!)[0]
        //
        let idVideo = (Video.getVideoByIdChapter(currChapter.chapter_id!)[0]).video_id
        let mVideo = Video.getVideoOfflineByIdVideo(idVideo)
        //
        let link = mVideo.video_url_offline
        let destinationURL = URL(fileURLWithPath: link)
        let fileManager = FileManager.default
        do {
            try fileManager.removeItem(at: destinationURL as URL)
        } catch {
            // Non-fatal: file probably doesn't exist
        }
        
        _ = Video.removeUrlOfflineVideo(myVideo.video_id)
        
        //---------------------------------
//        let myVideo = Video.getVideoByIdChapter(currChapter.chapter_id!)[0]
//        let idVideo = myVideo.video_id
        let urlOffline = Video.getVideoOfflineByIdVideo(idVideo)
        print(urlOffline)

        //---------------------------------
        // MARK: remove chapter from table ChapterOffline
        Chapter.removeOfflineChapter(currChapter)// remove chapter in table chapter offline by object curChapter
        if Video.getVideoByIdChapter(currChapter.chapter_id!).count > 0 {
            let myVideo = Video.getVideoByIdChapter(currChapter.chapter_id!)[0]
            _ = Video.removeUrlOfflineVideo(myVideo.video_id)
            
        }
        
        // post notifi để  reload lại table trang homeView sau khi remove khỏi bảng save offline
        NotificationCenter.default.post(name: Notification.Name(rawValue: "notiUpdateDataOffline"), object: nil)
        
        tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.automatic) // remove row in tableView at indexPath
        
        // check number of chapter in lesson offline after remove chapter to remove header section
        let numberOfChapters = Chapter.getAllChaptersOfflineByIdLesson(arrIdLessons[indexPath.section]).count
        if numberOfChapters == 0 {
            //tableView.deleteSections(IndexSet(indexPath.section), with: .automatic)
            
            DispatchQueue.main.async { [weak self] in
                guard let strongSelf = self else { return }
                strongSelf.arrIdLessons.remove(at: indexPath.section)
                tableView.deleteSections([indexPath.section], with: .automatic)
                // tableView.reloadData()
            }
        }
    }
    
//    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
//        
//    }
//    

    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .delete
    }
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "Xoá"
    }
}

