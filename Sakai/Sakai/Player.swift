//
//  Player.swift
//  Sakai
//
//  Created by dang hung on 12/6/16.
//  Copyright © 2016 Team 4. All rights reserved.
//

import UIKit
import AVFoundation



@objc protocol PlayerDelegate:class {
    
    @objc optional func playerDidFinish(player:AVPlayer?)
    
    
    @objc optional func playerDidFailed()
    
    @objc optional func playerItemDidChangeRate(player:AVPlayer?)
    
    @objc optional func playerItemKeepUpTo()
    
    @objc optional func playerItmeBufferEmpty(isKeepUp : Bool)
    
    @objc optional func playerItemReadyToPlay(player:AVPlayer?)
    
    
}




class Player: NSObject {
    
    var player:AVPlayer?
    var playerItem:AVPlayerItem?
    
    var isTracked = 0
    
    var filePath:String? {
        didSet {
            self.setupAsset()
        }
    }
    
    
    weak var delegate:PlayerDelegate?
    
    deinit {
        print("im is player im die.......")
    }
}

extension Player{
    
    
    func setupAsset() {
        
        if self.filePath != nil {
            let items = AVPlayerItem(url: URL(string: filePath!)!)
            player = AVPlayer(playerItem: items)
            
            prepareToPlayAsset()
        }
    }
    
    
    
    func prepareToPlayAsset() {
        
        
        self.player?.seek(to: CMTimeMakeWithSeconds(0, Int32(NSEC_PER_SEC)))
        self.registerPlayerItemObserver()
        
        
    }
    
}

//MARK: Add observer Player
extension Player {
    
    func registerPlayerItemObserver() {
        
        player?.addObserver(self, forKeyPath: "currentItem.playbackLikelyToKeepUp",
                            options: .new, context: &isTracked)
    }
    
    func deallocPlayerItemObserver() {
        player?.removeObserver(self, forKeyPath: "currentItem.playbackLikelyToKeepUp")
        
    }
    
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if context == &isTracked {
            if (player?.currentItem!.isPlaybackLikelyToKeepUp)! {
                delegate?.playerItemKeepUpTo!()
                delegate?.playerItmeBufferEmpty!(isKeepUp: false)
            } else {
                delegate?.playerItmeBufferEmpty!(isKeepUp: true)
                
            }
            if (player?.currentItem?.isPlaybackBufferEmpty)!{
                delegate?.playerItmeBufferEmpty!(isKeepUp: true)
                
            }
            
        }
    }
    
    func registerNotification() {
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(playerDidFinish),
                                               name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
                                               object: self.playerItem)
        
    }
    func removeNotification() {
        
        NotificationCenter.default.removeObserver(self,
                                                  name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
                                                  object: self.playerItem)
        
    }
    
    
    func playerDidFinish()  {
        delegate?.playerDidFinish!(player: player)
    }
    
}
