//
//  Helper.swift
//  Sakai
//
//  Created by Thien Lan on 11/24/16.
//  Copyright © 2016 Team 4. All rights reserved.
//

import Foundation
import UIKit

class Helper{
    
    private init(){}
    
    static var shareInstance = Helper()
    
    func showAlertTwoButton(from view:UIViewController, title:String, message:String, actions:@escaping ()->Void) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "NO", style: UIAlertActionStyle.cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "YES", style: UIAlertActionStyle.default, handler: { (action) in
            actions()
        }))
        view.present(alert, animated: true, completion: nil)
    }
    
    func showAlertSingleButton(from view:UIViewController, title:String, message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "YES", style: UIAlertActionStyle.cancel, handler: nil))
        view.present(alert, animated: true, completion: nil)
    }
}
