//
//  CustomPlayer.swift
//  Sakai
//
//  Created by dang hung on 12/6/16.
//  Copyright © 2016 Team 4. All rights reserved.
//

import UIKit
import AVFoundation


class CustomPlayer: BaseView{
    
    @IBOutlet weak var playerView: PlayerView!
    
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var btnPreviours: UIButton!
    @IBOutlet weak var btnFullScreen: UIButton!
    @IBOutlet weak var lbDuration: UILabel!
    @IBOutlet weak var lbTime: UILabel!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var seekbar: UISlider!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    @IBOutlet weak var scubberView: UIView!
    @IBOutlet weak var controlView: UIView!
    
    var m_player:Player?
    var timer:Timer?
    var filepath:String?
    
    
    var playerViewFullScreen: Bool = false
    
    override func awakeFromNib() {
        
        
    }
    
    deinit {
        print("Im....is customPlayer ... die")
    }
    
    func startPlayer() {
        setupPlayer()
        
        m_player = Player()
        m_player?.filePath = filepath
        m_player?.delegate = self
        m_player?.player?.play()

        self.btnPlay.setImage(UIImage(named: "icon_pause"), for: .normal)

        seekbar.minimumValue = 0
        seekbar.isContinuous = true
        seekbar.addTarget(self, action: #selector(playbackSliderValueChanged), for: .valueChanged)
        
    }
    //MARK: Handler btn Play
    
    @IBAction func btnPlayClicked(_ sender: AnyObject) {
        if m_player != nil {
            if rate > 0 { // playing
                self.btnPlay.tag = 1
                self.m_player?.player?.pause()
                self.btnPlay.setImage(UIImage(named: "icon_play"), for: .normal)
                
            }else {
                self.btnPlay.tag = 0
                self.m_player?.player?.play()
                self.btnPlay.setImage(UIImage(named: "icon_pause"), for: .normal)

            }
        }else {
            self.startPlayer()
            
        }
    }
    //MARK: Hide Loading view
    func hiddenLoadingView(flag:Bool) {
        if flag == false {
            loading.startAnimating()
        }
        self.loading.isHidden = flag
    }
    
    //MARK: Setup player
    func setupPlayer() {
        self.hiddenPlayerControl(flag: true)
        self.hiddenLoadingView(flag: false)
    }
    
    // MARK: Handler tap player view
    @IBAction func tapedPlayerView(sender: AnyObject) {
        if loading.isHidden == true { // loading is hiden
            self.hiddenPlayerControl(flag: !self.controlView.isHidden)
            
           
           
        }
        
        
       
    }
    
    //MARK: Hide Player Control
    
    func hiddenPlayerControl(flag:Bool) {
        
        UIView.animate(withDuration: 0.5, animations: {
            if flag == true {
                self.controlView.alpha = 0.5
                self.scubberView.alpha = 0.5
                self.titleView.alpha = 0.5
            }else {
                self.titleView.alpha = 1.0
                self.controlView.alpha = 1.0
                self.scubberView.alpha = 1.0
            }
        }) { (finish) in
            self.controlView.isHidden = flag
            self.scubberView.isHidden = flag
            self.titleView.isHidden = flag
            print(flag)
            if flag == false{
                let when = DispatchTime.now() + 5 // change 5 to desired number of seconds
                DispatchQueue.main.asyncAfter(deadline: when) {
                    self.hiddenPlayerControl(flag: true)
                    
                }
            }
            
        }
    }
}

//MARK: Player Delegate Handler
extension CustomPlayer: PlayerDelegate{
    

    
    func playerItemKeepUpTo() {
        self.playerView.player = self.m_player?.player
        initTimer()
        
        
    }
    
    func playerItmeBufferEmpty(isKeepUp: Bool) {
        
        if isKeepUp == true{
            hiddenLoadingView(flag: false)
        }else{
            hiddenLoadingView(flag: true)
            
        }
        
    }
    func playerDidFinish(player: AVPlayer?) {
        print("tao la player, tao play xong roi")
    }
    
}

//MARK: Handler Sync Time
extension CustomPlayer{
    
    var rate: Float {
        get {
            return (m_player?.player!.rate)!
        }
        set {
            m_player?.player!.rate = newValue
        }
    }
    var duration: Double { //get duration
        guard let currentItem = m_player?.player!.currentItem else { return 0.0 }
        return CMTimeGetSeconds(currentItem.duration)
    }
    
    var currentTime: Double { //get current time
        if ((self.m_player?.player!.currentTime()) != nil){
            return  CMTimeGetSeconds((self.m_player?.player!.currentTime())!)
            
        }
        return 0.0
    }
    
    //MARK: init Timer
    func initTimer() {
        
        let duration:Double = self.duration
        self.lbDuration.text = self.stringWithtimeInterval(interval: duration)
        
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(syncTime), userInfo: nil, repeats: true)
        
    }
    
    func syncTime() {
        
        let minValue:Float = self.seekbar.minimumValue
        let maxValue:Float = self.seekbar.maximumValue
        
        let delta:Float = (maxValue - minValue) * Float(self.currentTime) / Float(self.duration) + minValue
        let passTime:String = self.stringWithtimeInterval(interval: self.currentTime)
        
        self.lbTime.text = passTime
        self.seekbar.setValue(delta, animated: true)
        
    }
//MARK: convert timeinterval
    func stringWithtimeInterval(interval:Double) -> String {
        
        let durationDate = NSDate(timeIntervalSince1970: interval)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
        return dateFormatter.string(from: durationDate as Date)
    }
    
    //MARK: Handler sliderbar
    func playbackSliderValueChanged()  {
        
        let seekbarValue:Float = self.seekbar.value
        let minValue:Float = Float(self.seekbar.minimumValue)
        let maxValue:Float = self.seekbar.maximumValue
        let position:Double = Double((Float(self.duration) * (seekbarValue - minValue)) / (maxValue - minValue))
        self.m_player?.player?.seek(to: CMTimeMakeWithSeconds(position, Int32(NSEC_PER_SEC)))
        
        self.m_player?.player?.play()
    }
    //MARK: Handler Full Screen
    @IBAction func click_fullScreen(sender: AnyObject) {
        if AppDelegate.isFullScreen == false{
            AppDelegate.isFullScreen = true
            let value = UIInterfaceOrientation.landscapeLeft.rawValue
            UIDevice.current.setValue(value, forKey: "orientation")
        }else {
            AppDelegate.isFullScreen = false
            let value = UIInterfaceOrientation.portrait.rawValue
            UIDevice.current.setValue(value, forKey: "orientation")
        }
        self.setFullScreenView()
    }
    func setFullScreenView() {
        if playerViewFullScreen == true {
            self.titleView.isHidden = false
            self.controlView.isHidden = false
        }else {
            self.titleView.isHidden = true
            self.controlView.isHidden = true
        }
    }
    
    
}

//MARK: Handler Release Player
extension CustomPlayer {

    func releasePlayer()  {
        m_player?.player?.pause()
        m_player?.deallocPlayerItemObserver()
        timer?.invalidate()
        m_player = nil
    }
}
