//
//  dataManager.swift
//  Sakai_demo
//
//  Created by Developer on 11/24/16.
//  Copyright © 2016 Team 4. All rights reserved.
//

import UIKit
import Alamofire
class dataManager: NSObject {
    // lấy data từ chuỗi Json
    
    static var sharedInstance:dataManager!
    
    
    class func defaultDataManager() -> dataManager{
        if sharedInstance == nil {//Neu doi tuong chua ton tai
            //Khoi tao doi tuong
            sharedInstance = dataManager()
            sharedInstance.saveDataFromJSonToSQLite()
        }
        return sharedInstance
    }
    
    //*------------------------------------------
    func saveDataFromJSonToSQLite() {
        //
        _ = tableManager.checkExistSqlite()
        //
        typealias JSONSDicStandard = [String:AnyObject]
        typealias JSONSArrStandard = [AnyObject]
        //
        let strUrlPlaceHolderForImage = "resources/logo/sakai.jpg"
        let strPlaceHolderContent = "Chưa có nội dung"
        //
        var arrLessons = [Lesson]() // mảng chứa các lesson lấy từ Json
        var arrChapters = [Chapter]()
        var arrVideos = [Video]()
        //
        Alamofire.request(urlAPIGetAllCourse).responseJSON { response in
            if let JSON = response.result.value {
                let arr_Lessons = JSON as! JSONSArrStandard
                for lesson in arr_Lessons {
                    let newLesson = Lesson()
                    newLesson.lesson_id = lesson["id"] as? String ?? ""
                    newLesson.lesson_name = lesson["name"] as? String ?? ""
                    newLesson.lesson_logoPath = lesson["logoPath"] as? String ?? strUrlPlaceHolderForImage
                    //
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "d/MM/yyyy"
                    dateFormatter.locale = NSLocale(localeIdentifier: "vi_VN") as Locale!
                    let stringDate = dateFormatter.string(from: Date())
                    let date = dateFormatter.date(from: stringDate)
                    newLesson.lesson_dateStart = lesson["dateStart"] as? Date ?? date
                    //
                    newLesson.lesson_teacher = lesson["teacher"] as? String ?? ""
                    newLesson.lesson_description = lesson["description"] as? String ?? strPlaceHolderContent
                    //
                    arrLessons.append(newLesson)
                    
                    //-----------------------------------
                    //chapter
                    let listNode_Chapter = lesson["listNode"] as? JSONSArrStandard
                    if let listNode_Chapter = listNode_Chapter {
                        for chapter in listNode_Chapter {
                            let myChapter = Chapter()
                            myChapter.chapter_id = chapter["id"] as? String? ?? ""
                            myChapter.chapter_lesson_id = newLesson.lesson_id
                            myChapter.chapter_name = chapter["name"] as? String? ?? ""
                            myChapter.chapter_teacher = newLesson.lesson_teacher
                            myChapter.chapter_dateStart = newLesson.lesson_dateStart
                            myChapter.chapter_description = chapter["description"] as? String? ?? strPlaceHolderContent
                            myChapter.chapter_logoPath = newLesson.lesson_logoPath
                            //
                            arrChapters.append(myChapter)
                            
                            //-----------------------Video
                            let listNode_Video = chapter["videoList"] as? JSONSArrStandard
                            if let listNode_Video = listNode_Video {
                                for video in listNode_Video {
                                    let myVideo = Video()
                                    myVideo.video_id = video["id"] as? String ?? ""
                                    myVideo.video_chapter_id = myChapter.chapter_id!
                                    myVideo.video_name = video["videoName"] as? String ?? ""
                                    myVideo.video_type = (video["videoType"] as! JSONSDicStandard)["type"] as! String
                                    let url = video["url"] as! JSONSArrStandard
                                    myVideo.video_url = url[0] as? String ?? ""
                                    //
                                    arrVideos.append(myVideo)
                                }
                                //
                                Video.saveVideosToSQLite(arrVideos)
                                arrVideos.removeAll() // remove all element after save to sqlite
                            }
                        }
                        //
                        Chapter.saveChaptersToSQLite(arrChapters)
                        arrChapters.removeAll() // remove all element after save to sqlite
                    }
                }
                Lesson.saveLessonsToSQLite(arrLessons)
                let myDict = ["data":arrLessons]
                NotificationCenter.default.post(name: Notification.Name(rawValue: "notiFinishLoadData"), object: nil, userInfo: myDict)
                arrLessons.removeAll() // remove all element after save to sqlite
            }
        }
    }
}

