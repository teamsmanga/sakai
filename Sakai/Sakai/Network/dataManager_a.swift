//
//  dataManager.swift
//  Sakai_demo
//
//  Created by Developer on 11/24/16.
//  Copyright © 2016 Team 4. All rights reserved.
//

import UIKit
import Alamofire
class dataManager: NSObject {
    // lấy data từ chuỗi Json
    
    static var sharedInstance:dataManager!
    
    
    class func defaultDataManager() -> dataManager{
        if sharedInstance == nil {//Neu doi tuong chua ton tai
            //Khoi tao doi tuong
            sharedInstance = dataManager()
            sharedInstance.saveDataFromJSonToSQLite()
        }
        return sharedInstance
    }
    
    //*------------------------------------------
    func saveDataFromJSonToSQLite() {
        //
        _ = tableManager.checkExistSqlite()
        //
        typealias JSONSDicStandard = [String:AnyObject]
        typealias JSONSArrStandard = [AnyObject]
        let strPlaceHolder = "chưa có nội dung" // string placeholder when value is null
        //
        var arrLessons = [Lesson]() // mảng chứa các lesson lấy từ Json
        var arrChapters = [Chapter]()
        var arrVideos = [Video]()
        //
        var dataList_Lesson:[String:AnyObject] = [:]
        //
        let linkAPI = "http://gst.fsoft.com.vn/Elearning-App/user/findOneById/58253761a826fb5334af72d8"
        Alamofire.request(linkAPI).responseJSON { response in
            //
            if let JSON = response.result.value {
                dataList_Lesson = JSON as! JSONSDicStandard
                let arr_lesson = dataList_Lesson["myNode"] as! JSONSArrStandard
                // lesson
                for lesson in arr_lesson {
                    let newLesson = Lesson()
                    newLesson.lesson_id = lesson["id"] as? String? ?? strPlaceHolder
                    newLesson.lesson_user_id = "fsofter"
                    
                    newLesson.lesson_name = lesson["name"] as? String? ?? strPlaceHolder
                    newLesson.lesson_teacher = lesson["teacher"] as? String? ?? strPlaceHolder
                    newLesson.lesson_description = lesson["description"] as? String? ?? strPlaceHolder
                    newLesson.lesson_logoPath = lesson["logoPath"] as? String? ?? ""
                    //
                    arrLessons.append(newLesson) // thêm lesson vào mảng arrLessons để thực hiện trả về
                    //chapter
                    let arr_chapter = lesson["listNode"] as? JSONSArrStandard
                    if let arr_chapter = arr_chapter {
                        for chapter in arr_chapter {
                            let newChapter = Chapter()
                            newChapter.chapter_id = chapter["id"] as? String? ?? strPlaceHolder
                            newChapter.chapter_lesson_id = newLesson.lesson_id
                            newChapter.chapter_name = chapter["name"] as? String? ?? strPlaceHolder
                            newChapter.chapter_teacher = newLesson.lesson_teacher
                            newChapter.chapter_dateStart = newLesson.lesson_dateStart
                            newChapter.chapter_description = chapter["description"] as? String? ?? strPlaceHolder
                            newChapter.chapter_logoPath = newLesson.lesson_logoPath
                            //
                            arrChapters.append(newChapter) // thêm lesson vào mảng arrChapters để thực hiện trả về
                            //Video
                            let arr_video = chapter["videoList"] as? JSONSArrStandard
                            if let arr_video = arr_video {
                                for video in arr_video {
                                    let newVideo = Video()
                                    newVideo.video_id = video["id"] as? String ?? strPlaceHolder
                                    newVideo.video_chapter_id = newChapter.chapter_id!
                                    newVideo.video_name = video["videoName"] as? String ?? strPlaceHolder
                                    newVideo.video_type = (video["videoType"] as! JSONSDicStandard)["type"] as! String
                                    let url = video["url"] as! JSONSArrStandard
                                    newVideo.video_url = url[0] as? String ?? strPlaceHolder
                                    //
                                    arrVideos.append(newVideo)
                                }
                                //
                                Video.saveVideosToSQLite(arrVideos)
                                arrVideos.removeAll() // remove all element after save to sqlite
                            }
                        }
                        //
                        Chapter.saveChaptersToSQLite(arrChapters)
                        arrChapters.removeAll() // remove all element after save to sqlite
                    }
                }
                //
                let myDict = ["data":arrLessons]
                Lesson.saveLessonsToSQLite(arrLessons)
                NotificationCenter.default.post(name: Notification.Name(rawValue: "notiFinishLoadData"), object: nil, userInfo: myDict)
                arrLessons.removeAll() // remove all element after save to sqlite
                //
            }
        }
    }
}
