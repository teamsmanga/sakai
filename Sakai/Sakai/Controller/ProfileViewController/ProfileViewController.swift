//
//  ProfileViewController.swift
//  Sakai
//
//  Created by Developer on 11/29/16.
//  Copyright © 2016 Team 4. All rights reserved.
//

import UIKit
import QuartzCore

class ProfileViewController: UIViewController {
    //*-----------------------------
    @IBOutlet weak var profileNavBar: UINavigationBar!
    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var lblSoDienThoai: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblFacebook: UILabel!
    @IBOutlet weak var lblHoTen: UILabel!
    
    @IBAction func doneProfileNav(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    //*-----------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        //
        setStatusBarBackgroundColor(color: orangeSwift)
        // thay đổi background Nav Bar
        profileNavBar.isTranslucent = false
        profileNavBar.barTintColor = orangeSwift
        profileNavBar.tintColor = UIColor.white
        profileNavBar.barStyle = UIBarStyle.blackTranslucent
        profileNavBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        // gán thông tin người dùng vào các label
//        lblHoTen.text = 
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //
        let userDefault = UserDefaults.standard
        lblEmail.text = userDefault.string(forKey: "EmailUser")
        lblHoTen.text = userDefault.string(forKey: "NameUser")
        lblSoDienThoai.text = userDefault.string(forKey: "PhoneUser")
        lblFacebook.text = userDefault.string(forKey: "FBUser")
        // image profile
        imgAvatar.layer.cornerRadius = imgAvatar.frame.size.width/2;
        imgAvatar.clipsToBounds = true
        imgAvatar.layer.borderWidth = 3.0;
        imgAvatar.layer.borderColor = UIColor.white.cgColor
        imgAvatar.layer.cornerRadius = 10.0;
        imgAvatar.layer.shadowColor = UIColor.black.cgColor
        imgAvatar.layer.shadowOffset = CGSize(width:3.0,height: 3.0)
        imgAvatar.layer.shadowRadius = 10.0
        imgAvatar.layer.shadowOpacity = 1.0
        imgAvatar.layer.masksToBounds = false;
        
        // tạo image thành hình tròn 
        var layer: CALayer = CALayer()
        layer = imgAvatar.layer
        layer.masksToBounds = true
        layer.cornerRadius = imgAvatar.frame.size.width/2
        UIGraphicsBeginImageContext(imgAvatar.bounds.size)
        layer.render(in: UIGraphicsGetCurrentContext()!)
        UIGraphicsEndImageContext()
        
        
    }
    
    // change background color statusBar
    func setStatusBarBackgroundColor(color: UIColor) {
        guard let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else { return }
        statusBar.backgroundColor = orangeSwift
    }
    
    // change text in statusBar to lightcontent
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnDismiss(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
