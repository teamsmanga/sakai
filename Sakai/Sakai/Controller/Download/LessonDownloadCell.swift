//
//  LessonDownloadCell.swift
//  Sakai
//
//  Created by PhatLe on 12/1/16.
//  Copyright © 2016 Team 4. All rights reserved.
//

import UIKit

class LessonDownloadCell: UITableViewCell {

    @IBOutlet weak var lbNameLesson: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.backgroundColor = orangeLesson
        lbNameLesson.textColor = #colorLiteral(red: 0.2196078449, green: 0.007843137719, blue: 0.8549019694, alpha: 1)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
