//
//  DownloadViewController.swift
//  Sakai
//
//  Created by PhatLe on 11/23/16.
//  Copyright © 2016 Team 4. All rights reserved.
//

import UIKit
import AVKit
import MediaPlayer
import UserNotifications

class DownloadViewController: UIViewController {
    
    var listCourse = [Course]()
    var listLesson = [LessonD]()
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var segControl: UISegmentedControl!
    @IBAction func seg(_ sender: Any) {
        switch (segControl.selectedSegmentIndex) {
        case 0:
            print("Downloaded")
            tableView.register(UINib(nibName: "CourseDownloadCell", bundle: nil), forCellReuseIdentifier: "CourseDownloadCell")
            tableView.reloadData()
        case 1:
            print("Queue")
            tableView.register(UINib(nibName: "LessonQueueTableViewCell", bundle: nil), forCellReuseIdentifier: "LessonQueueTableViewCell")
            tableView.reloadData()
        default:
            break
        }
    }
    
    //Download
    //====================================================================================
    
    let defaultSession = URLSession(configuration: URLSessionConfiguration.default)
    var dataTask: URLSessionDataTask?
    var activeDownloads = [String: Download]()
    
    lazy var downloadsSession: URLSession = {
        // instead of using the default session configuration, you use a special background session configuration
        // you also set a unique identifier for the session here to allow you to reference and "reconnect" to the same background session if needed
        let configuration = URLSessionConfiguration.background(withIdentifier: "bgSessionConfiguration")
        let session = URLSession(configuration: configuration, delegate: self, delegateQueue: nil)
        return session
    }()
    
    //====================================================================================
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        listCourse.append(Course(nameCourse: "Amazon Webservice", imageCourse: "aws.jpg", nameTeacher: "Mark", numberLesson: 2))
        listCourse.append(Course(nameCourse: "Triển khai hệ thống đào tạo trực tuyến", imageCourse: "sakai.jpg", nameTeacher: "ThachLN", numberLesson: 2))
        listLesson.append(LessonD(nameLesson: "Introduction", type: "Lecture", preUrl: "http://khoapham.vn/download/amthambenem.mp4"))
        listLesson.append(LessonD(nameLesson: "1.Domain-1-High-Availability-and-Business-Continuity", type: "Lecture", preUrl: "https://clc.fsoft.com.vn/vod/aws/AcloudGuru_AWS-CSA-PRO/1.Domain-1-High-Availability-and-Business-Continuity/1.DR-&-AWS-Part-1.mp4"))
        listLesson.append(LessonD(nameLesson: "Triển khai hệ thống học tập trực tuyến", type: "Lecture", preUrl: "http://cdn.liveclips.net/2014/ftb.nfl/56421/0152_079717c7835da1064561bc0b3f47404a1327811f/07971_directv_00_other.mp4"))
        listLesson.append(LessonD(nameLesson: "Giới thiệu hệ thống sakai", type: "Lecture", preUrl: "http://mks.com.vn/guideline/sakai/01"))
        
        // Do any additional setup after loading the view.
        
        UNUserNotificationCenter.current().delegate = self
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.layer.borderWidth = 4.0
        tableView.layer.borderColor = UIColor.orange.cgColor
        switch segControl.selectedSegmentIndex {
        case 0:
            tableView.register(UINib(nibName: "CourseDownloadCell", bundle: nil), forCellReuseIdentifier: "CourseDownloadCell")
        case 1:
            tableView.register(UINib(nibName: "LessonQueueTableViewCell", bundle: nil), forCellReuseIdentifier: "LessonQueueTableViewCell")
        default:
            tableView.register(UINib(nibName: "CourseDownloadCell", bundle: nil), forCellReuseIdentifier: "CourseDownloadCell")
        }
        _ = self.downloadsSession
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    //Download
    func startDownload(lesson: LessonD) {
        // TODO
        if let urlString = lesson.preUrl, let url = URL(string: urlString) {
            // initialize a Download with the preview URL of the track
            let download = Download(url: urlString)
            // using your new session object, you create a URLSessionDownloadTask with the preview URL and set it to the downloadTask property of the Download
            download.downloadTask = downloadsSession.downloadTask(with: url)
            // start the download task by calling resume() on it
            download.downloadTask!.resume()
            // indicate that the download is in progress
            download.isDownloading = true
            // finally map the download URL to its Download in the activeDownloads dictionary
            activeDownloads[download.url] = download
        }
    }
    
    // Called when the Pause button for a track is tapped
    func pauseDownload(lesson: LessonD) {
        // TODO
        if let urlString = lesson.preUrl, let download = activeDownloads[urlString] {
            if download.isDownloading {
                // you retrieve the resume data from the closure provided by cancel(byProducingResumeData:)
                // and save it to the appropriate Download for future resumption
                
                download.downloadTask?.suspend()
                // set isDownloading to false, to signify that the download is paused
                download.isDownloading = false
            }
        }
    }
    
    // Called when the Cancel button for a track is tapped
    func cancelDownload(lesson: LessonD) {
        // TODO
        if let urlString = lesson.preUrl, let download = activeDownloads[urlString] {
            // call cancel on the corresponding Download in the dictionary of active downloads
            download.downloadTask?.cancel()
            // you then remove it from the dictionary of active downloads
            activeDownloads[urlString] = nil
        }
    }
    
    func resumeDownload(lesson: LessonD) {
        // TODO
        if let urlString = lesson.preUrl, let download = activeDownloads[urlString] {
            download.downloadTask?.resume()
            download.isDownloading = true
        }
        
    }
    
    //    func playDownload(lesson: Lesson) {
    //        if let urlString = lesson.preUrl, let url = localFilePathForUrl(preUrl: urlString) {
    //            let player = AVPlayer(url: url as URL)
    //            let playerViewController = AVPlayerViewController()
    //            playerViewController.player = player
    //            self.present(playerViewController, animated: true) {
    //                playerViewController.player!.play()
    //            }
    //        }
    //    }
    
    func localFilePathForUrl(preUrl: String) -> NSURL? {
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString
        if let url = NSURL(string: preUrl), let lastPathComponent = url.lastPathComponent {
            let fullPath = documentsPath.appendingPathComponent(lastPathComponent)
            return NSURL(fileURLWithPath:fullPath)
        }
        return nil
    }
    
    func localFileExistsForClip(lesson: LessonD) -> Bool {
        if let urlString = lesson.preUrl, let localUrl = localFilePathForUrl(preUrl: urlString) {
            var isDir : ObjCBool = false
            if let path = localUrl.path {
                return FileManager.default.fileExists(atPath: path, isDirectory: &isDir)
            }
        }
        return false
    }
    
    func lessonIndexForDownloadTask(downloadTask: URLSessionDownloadTask) -> Int? {
        if let url = downloadTask.originalRequest?.url?.absoluteString {
            for (index, lesson) in listLesson.enumerated() {
                if url == lesson.preUrl {
                    return index
                }
            }
        }
        return nil
    }
    
}

extension DownloadViewController: UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var numberRow = 0
        switch (segControl.selectedSegmentIndex) {
        case 0:
            numberRow = listCourse.count
        case 1:
            numberRow = listLesson.count
        default:
            break
        }
        return numberRow
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if segControl.selectedSegmentIndex == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CourseDownloadCell", for: indexPath) as! CourseDownloadCell
            let course = listCourse[indexPath.row]
            cell.lbNameCourse.text = course.nameCourse
            cell.lbNameTeacher.text = "Teacher : " + course.nameTeacher!
            cell.imgCourse.image = UIImage(named: course.imageCourse!)
            cell.lbNumberLessonDownloaded.text = "2 " + "Lesson Downloaded"
            return cell
        } else {
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "LessonQueueTableViewCell", for: indexPath) as! LessonQueueTableViewCell
            let lesson = listLesson[indexPath.row]
            cell1.delegate = self
            cell1.lbNameLesson.text = lesson.nameLesson
            
            var showDownloadControls = false
            if let download = activeDownloads[lesson.preUrl!] {
                showDownloadControls = true
                
                cell1.progressView.progress = download.progress
                cell1.lbProgress.text = (download.isDownloading) ? "Downloading..." : "Paused"
                
                // this toggles the button between the two states pause and resume
                let title = (download.isDownloading) ? "Pause" : "Resume"
                cell1.btnPause.setTitle(title, for: UIControlState.normal)
            }
            cell1.progressView.isHidden = !showDownloadControls
            cell1.lbProgress.isHidden = !showDownloadControls
            
            // If the track is already downloaded, enable cell selection and hide the Download button
            let downloaded = localFileExistsForClip(lesson: lesson)
            cell1.selectionStyle = downloaded ? UITableViewCellSelectionStyle.gray : UITableViewCellSelectionStyle.none
            
            // hide the Download button also if its track is downloading
            cell1.btnDownload.isHidden = downloaded || showDownloadControls
            
            // show the pause and cancel buttons only if a download is active
            cell1.btnPause.isHidden = !showDownloadControls
            cell1.btnCancel.isHidden = !showDownloadControls
            return cell1
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(100)
    }
    
}

extension DownloadViewController: URLSessionDownloadDelegate{
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        // extract the original request URL from the task and pass it to the provided localFilePathForUrl(_:) helper method.
        // localFilePathForUrl(_:) then generates a permanent local file path to save to by appending the lastPastComponent of the URL
        // (i.e. the file name and extension of the file) to the path of the app's Documents directory
        if let originalURL = downloadTask.originalRequest?.url?.absoluteString, let destinationURL = localFilePathForUrl(preUrl: originalURL) {
            print(destinationURL)
            
            // with FileManager you move the downloaded file from its temporary file location to the desired destination file path by
            // clearing out any item at that location before you start the copy task
            let fileManager = FileManager.default
            do {
                try fileManager.removeItem(at: destinationURL as URL)
            } catch {
                // Non-fatal: file probably doesn't exist
            }
            do {
                try fileManager.copyItem(at: location, to: destinationURL as URL)
                LocalNotification.shareInstance.pushnotiwhendownloadcompleted(urlLesson: originalURL)
            } catch let error as NSError {
                print("Could not copy file to disk: \(error.localizedDescription)")
            }
        }
        
        // look up the corresponding Download in your active downloads and remove it
        if let url = downloadTask.originalRequest?.url?.absoluteString {
            activeDownloads[url] = nil
            // look up the Track in your table view and reload the corresponding cell
            if let lessonIndex = lessonIndexForDownloadTask(downloadTask: downloadTask) {
                DispatchQueue.main.async {[weak self] in
                    self?.tableView.reloadRows(at: [IndexPath(row: lessonIndex, section: 0)], with: .none)
                }
            }
        }
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        // using the provided downloadTask, you extract the URL and use it to find the Download in your dictionary of active downloads.
        if let downloadUrl = downloadTask.originalRequest?.url?.absoluteString, let download = activeDownloads[downloadUrl] {
            // method returns total bytes written and the total bytes expected to be written. You calculate the progress as the ratio of the two
            // values and save the result in the Download. You'll use this value to update the progress view.
            download.progress = Float(totalBytesWritten)/Float(totalBytesExpectedToWrite)
            // ByteCountFormatter takes a byte value and generates a human-readable string showing the total download file size. You'll use this string to show the size of the download alongside the percentage complete
            let totalSize = ByteCountFormatter.string(fromByteCount: totalBytesExpectedToWrite, countStyle: ByteCountFormatter.CountStyle.binary)
            // find the cell responsible for displaying the Track and update both its progress view and progress label with the values derived form the previous steps
            if let lessonIndex = lessonIndexForDownloadTask(downloadTask: downloadTask), let lessonCell = tableView.cellForRow(at: IndexPath(row: lessonIndex, section: 0)) as? LessonQueueTableViewCell {
                DispatchQueue.main.async {
                    lessonCell.progressView.progress = download.progress
                    lessonCell.lbProgress.text = String(format: "%.1f%% of %@", download.progress * 100, totalSize)
                }
            }
        }
    }
    
    
}

extension DownloadViewController: CustomCellDelegate{
    func downloadTapped(_ cell:LessonQueueTableViewCell){
        if let indexPath = tableView.indexPath(for: cell) {
            let lesson = listLesson[indexPath.row]
            startDownload(lesson: lesson)
            tableView.reloadRows(at: [IndexPath(row: indexPath.row, section: 0)], with: .none)
        }
        
    }
    func resumeTapped(_ cell:LessonQueueTableViewCell){
        if let indexPath = tableView.indexPath(for: cell) {
            let lesson = listLesson[indexPath.row]
            resumeDownload(lesson: lesson)
            tableView.reloadRows(at: [IndexPath(row: indexPath.row, section: 0)], with: .none)
        }
    }
    func pauseTapped(_ cell:LessonQueueTableViewCell){
        if let indexPath = tableView.indexPath(for: cell) {
            let lesson = listLesson[indexPath.row]
            pauseDownload(lesson: lesson)
            tableView.reloadRows(at: [IndexPath(row: indexPath.row, section: 0)], with: .none)
        }
    }
    func cancelTapped(_ cell:LessonQueueTableViewCell){
        if let indexPath = tableView.indexPath(for: cell) {
            let lesson = listLesson[indexPath.row]
            cancelDownload(lesson: lesson)
            tableView.reloadRows(at: [IndexPath(row: indexPath.row, section: 0)], with: .none)
        }
    }
    
}

extension DownloadViewController:UNUserNotificationCenterDelegate {
    
    //for displaying notification when app is in foreground
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        //If you don't want to show notification when app is open, do something here else and make a return here.
        //Even you you don't implement this delegate method, you will not see the notification on the specified controller. So, you have to implement this delegate and make sure the below line execute. i.e. completionHandler.
        
        completionHandler([.alert,.badge])
    }
    
    // For handling tap and user actions
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        switch response.actionIdentifier {
        case "action":
            print("Action First Tapped")
        default:
            break
        }
        completionHandler()
    }
    
}
