//
//  Lesson.swift
//  Sakai
//
//  Created by PhatLe on 11/23/16.
//  Copyright © 2016 Team 4. All rights reserved.
//

import Foundation

class LessonD{
    var nameLesson: String?
    var type: String?
    var preUrl : String?

    init(nameLesson: String?, type: String?, preUrl: String?) {
        self.nameLesson = nameLesson
        self.type = type
        self.preUrl = preUrl
    }
}
