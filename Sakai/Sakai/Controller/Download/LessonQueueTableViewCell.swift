//
//  LessonQueueTableViewCell.swift
//  Sakai
//
//  Created by PhatLe on 11/23/16.
//  Copyright © 2016 Team 4. All rights reserved.
//

import UIKit

class LessonQueueTableViewCell: UITableViewCell {
    
    var delegate: CustomCellDelegate?
    
    @IBOutlet weak var lbNameLesson: UILabel!
    @IBOutlet weak var lbProgress: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var btnDownload: UIButton!
    @IBOutlet weak var btnPause: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBAction func pauseOrResumeTapped(_ sender: Any) {
        if(btnPause.titleLabel!.text == "Pause") {
            delegate?.pauseTapped(self)
        } else {
            delegate?.resumeTapped(self)
        }
    }
    @IBAction func downloadTapped(_ sender: Any) {
        delegate?.downloadTapped(self)
    }
    @IBAction func cancelTapped(_ sender: Any) {
        delegate?.cancelTapped(self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

protocol CustomCellDelegate {
    func downloadTapped(_ cell:LessonQueueTableViewCell)
    func resumeTapped(_ cell:LessonQueueTableViewCell)
    func pauseTapped(_ cell:LessonQueueTableViewCell)
    func cancelTapped(_ cell:LessonQueueTableViewCell)
}
