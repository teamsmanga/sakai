//
//  FakeData.swift
//  Sakai
//
//  Created by PhatLe on 11/23/16.
//  Copyright © 2016 Team 4. All rights reserved.
//

import Foundation
import UIKit

class Course{
    var nameCourse: String?
    var imageCourse: String?
    var nameTeacher: String?
    var numberLesson: Int?
 
    init(nameCourse: String?, imageCourse: String?, nameTeacher: String?, numberLesson: Int?) {
        self.nameCourse = nameCourse
        self.imageCourse = imageCourse
        self.nameTeacher = nameTeacher
        self.numberLesson = numberLesson
    }
}
