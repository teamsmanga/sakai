//
//  LocalNotification.swift
//  Sakai
//
//  Created by PhatLe on 11/25/16.
//  Copyright © 2016 Team 4. All rights reserved.
//

import Foundation
import UserNotifications

class LocalNotification{
    
    private init(){}
    
    static var shareInstance = LocalNotification()
    func pushnotiwhendownloadcompleted(urlLesson : String){
        let content = UNMutableNotificationContent()
        let requestIdentifier = "rajanNotification"
        
        content.badge = 1
        content.title = "Download is completed"
        content.subtitle = "Course"
        content.body = "Lesson1"
        content.categoryIdentifier = "actionCategory"
        content.sound = UNNotificationSound.default()
        
        let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 3.0, repeats: false)
        
        let request = UNNotificationRequest(identifier: requestIdentifier, content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request) { (error:Error?) in
            if error != nil {
                print(error?.localizedDescription)
            }     
            print("Notification Register Success")
        }
    }
    
    func registerForNotifications(){
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert,.badge,.sound]) { (granted:Bool, error:Error?) in
            if error != nil {
                print(error?.localizedDescription)
            }
            if granted {
                print("Permission granted")
            } else {
                print("Permission not granted")
            }
        }
        
        let action = UNNotificationAction(identifier: "action", title: "Download", options: [.foreground])
        
        let category = UNNotificationCategory(identifier: "actionCategory", actions: [action], intentIdentifiers: [], options: [])
        
        UNUserNotificationCenter.current().setNotificationCategories([category])

    }
}
