//
//  CourseDownloadCell.swift
//  Sakai
//
//  Created by PhatLe on 11/23/16.
//  Copyright © 2016 Team 4. All rights reserved.
//

import UIKit

class CourseDownloadCell: UITableViewCell {
    
    
    @IBOutlet weak var imgCourse: UIImageView!
    @IBOutlet weak var lbNameCourse: UILabel!
    @IBOutlet weak var lbNameTeacher: UILabel!
    @IBOutlet weak var lbNumberLessonDownloaded: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        self.layer.borderWidth = 4.0
        self.layer.addBorder(edge: UIRectEdge.top, color: .orange, thickness: 4.0)
        self.layer.addBorder(edge: UIRectEdge.bottom, color: .orange, thickness: 4.0)
        self.layer.borderColor = orangeSwift.cgColor
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
