//
//  ShowAllTableViewController.swift
//  Sakai
//
//  Created by Developer on 12/1/16.
//  Copyright © 2016 Team 4. All rights reserved.
//

import UIKit
private let kTableHeaderHeight:CGFloat = 300.0
private let ktableHeaderCutAway:CGFloat = 50.0
class ShowAllTableViewController: UITableViewController,  DownloadViewControllerDelegate {
    //
    var idLesson:String? = nil
    var arrChapter:[Chapter]? = nil
    var arrAllChaptersOffline:[Chapter]? = nil
    var dic_idChapter_idLesson = [String:String]()
    var isListChapterOfflineUpdated = false
    //
    let imageCheck = UIImage(named: "Check")
    //
    var headerMaskLayer:CAShapeLayer!
    //
    var headerView: UIView!
    //
    var progressView = UIProgressView()
    var progressDownload: Float?
    
    let downLoadVC = DownloadViewController()

    //
    override func viewDidLoad() {
        super.viewDidLoad()
        //
        arrChapter = Chapter.getChaptersByIdLesson(idLesson!)
        tableView.register(UINib(nibName:"Cell_ShowAll", bundle: nil), forCellReuseIdentifier: "cellShowAll")
        //
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 300
        //
        headerView = tableView.tableHeaderView
        tableView.tableHeaderView = nil
        tableView.addSubview(headerView)
        tableView.contentInset = UIEdgeInsets(top: kTableHeaderHeight, left: 0, bottom: 0, right: 0)
        tableView.contentOffset = CGPoint(x: 0, y: -kTableHeaderHeight)
        updateHeaderView()
        //
        headerMaskLayer = CAShapeLayer()
        headerMaskLayer.fillColor = UIColor.black.cgColor
        headerView.layer.mask = headerMaskLayer
        updateHeaderView()
        //
        let effectHeight = kTableHeaderHeight-ktableHeaderCutAway/2
        tableView.contentInset = UIEdgeInsets(top: effectHeight, left: 0, bottom: 0, right: 0)
        tableView.contentOffset = CGPoint(x: 0, y: -effectHeight)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        arrAllChaptersOffline = Chapter.getAllChaptersOffline()
        // tạo dic từ idchapter và lesson_chapter_id để kiểm tra record có trong table offline
        for item in arrAllChaptersOffline! {
            dic_idChapter_idLesson[item.chapter_id!] = item.chapter_lesson_id
        }
        tableView.reloadData()
    }
    
    func updateHeaderView() {
        //
        var headerRect = CGRect(x: 0, y: -kTableHeaderHeight, width: tableView.bounds.width, height: kTableHeaderHeight)
        if tableView.contentOffset.y < -kTableHeaderHeight {
            headerRect.origin.y = tableView.contentOffset.y
            headerRect.size.height = -tableView.contentOffset.y
        }
        headerView.frame = headerRect
        //
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 0, y: 0))
        path.addLine(to: CGPoint(x: headerRect.width, y: 0))
        path.addLine(to: CGPoint(x: headerRect.width, y: headerRect.height))
        path.addLine(to: CGPoint(x: 0, y: headerRect.height-ktableHeaderCutAway))
        headerMaskLayer?.path = path.cgPath
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        updateHeaderView()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (arrChapter?.count)! + 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // tạo cell table thông tin bài học
        if indexPath.row == 0 {
            tableView.register(UINib(nibName:"Cell_Description", bundle: nil), forCellReuseIdentifier: "Cell_Description")
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell_Description", for: indexPath) as? Cell_Description
            let lesson = Lesson.getLessonByIdLesson(idLesson!)
            cell?.lblTeacher.text = lesson.lesson_teacher
            cell?.lblDateStart.text = "04/12/2016"
            cell?.txtDescription.text = lesson.lesson_description
            cell?.selectionStyle = .none
            return cell!
        } else {
            // tạo cell các chapter trong lesson
            if isListChapterOfflineUpdated == true {
                arrAllChaptersOffline = Chapter.getAllChaptersOffline()
                // tạo dic từ idchapter và lesson_chapter_id để kiểm tra record có trong table offline
                dic_idChapter_idLesson.removeAll()
                for item in arrAllChaptersOffline! {
                    dic_idChapter_idLesson[item.chapter_id!] = item.chapter_lesson_id!
                }
                isListChapterOfflineUpdated = !isListChapterOfflineUpdated
            }
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellShowAll", for: indexPath) as? Cell_ShowAll
            if let cell = cell, let arrChapter = arrChapter {
                let currChapter = arrChapter[indexPath.row-1]
                cell.lblTitleChapter.text = String(format: "📔 %@", currChapter.chapter_name!)
                cell.lblTeacher.text = String(format: "🙍🏻‍♂️ %@", currChapter.chapter_teacher!)
                // check if value of key chapter_id not nil to make sure the chapter is offline and show status "downloaded"
                if let lesson_chapter_id = dic_idChapter_idLesson[currChapter.chapter_id!] {
                    if lesson_chapter_id == currChapter.chapter_lesson_id! {
                        cell.imgCheck.image = imageCheck
                    } else {
                        cell.imgCheck.image = nil
                    }
                } else {
                    cell.imgCheck.image = nil
                }
                // Gán logo mặc định khi ko load dc image trên url
                let img = UIImage(named: "sakai")
                cell.imgShowAllCell.image = img
                DispatchQueue.global().async {
                    let imgLogo = getImgFromUrl(URL(string: "http://gst.fsoft.com.vn/Elearning-App/" + currChapter.chapter_logoPath!)!)
                    DispatchQueue.main.async {
                        cell.imgShowAllCell.image = imgLogo
                    }
                }
                //
                cell.accessoryType = .disclosureIndicator
                return cell
            }
        }
        return UITableViewCell()
    }
    
    // push sang detailView của chapter khi select 1 row trong showAll chapter trong 1 lesson
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row > 0 {
            if isConnect == true {
                let story = UIStoryboard(name: "Main", bundle: nil)
                let lessonVC = story.instantiateViewController(withIdentifier: "lessonViewController") as? LessonViewController
                lessonVC?.idChapter = arrChapter?[indexPath.row-1].chapter_id
                self.navigationController?.pushViewController(lessonVC!, animated: true)
            } else {
                print("mat ket noi den may chu")
            }
        }
    }
    
    //thực hiện lưu offline bài học
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        let resultSaveChapter = Chapter.saveOfflineChapter((self.arrChapter?[indexPath.row-1])!)
        if resultSaveChapter == false { // lưu thất bại
            let alertController = UIAlertController(title: "Messages", message: "The lesson was saved before. The action is not available", preferredStyle: .alert)
            let closeAction = UIAlertAction(title: "Đóng", style: UIAlertActionStyle.cancel)
            //
            alertController.addAction(closeAction)
            self.present(alertController, animated: true, completion: nil)
        } else {
            isListChapterOfflineUpdated = !isListChapterOfflineUpdated
            
            // post notifi để reload table trang home khi thực hiện lưu bài học offline
            NotificationCenter.default.post(name: Notification.Name(rawValue: "notiUpdateDataOffline"), object: nil)
            //
            // thuc hien download video khi save offline bai hoc
            let currentVideo = Video.getVideoByIdChapter(self.arrChapter![indexPath.row-1].chapter_id!)
            if currentVideo[0].video_type == "HTML" {
                // lưu thành công
                let alertController = UIAlertController(title: "Messages", message: "The lesson is saved successfully", preferredStyle: .alert)
                let closeAction = UIAlertAction(title: "Đóng", style: UIAlertActionStyle.cancel) { (result : UIAlertAction) -> Void in
                    tableView.reloadRows(at: [indexPath], with: .none)
                }
                //
                alertController.addAction(closeAction)
                self.present(alertController, animated: true, completion: nil)
            } else {
                if currentVideo.count > 0 {
                    downLoadVC.nameChapter = "Saved successfully \(self.arrChapter?[indexPath.row-1].chapter_name)" // nội dung để push local notification
                    downLoadVC.startDownload(urlDownload: currentVideo[0].video_url)
                    // link download video bài học
                    downLoadVC.arrVideo = currentVideo
                    let alert = UIAlertController(title: "Message", message: "Downloading...", preferredStyle: .alert)
                    let margin:CGFloat = 8.0
                    let rect = CGRect(x: margin, y: 72.0, width: 250, height: 2.0)
                    progressView = UIProgressView(frame: rect)
                    progressView.setProgress(0.0, animated: true)
                    progressView.tintColor = UIColor.green
                    alert.view.addSubview(progressView)
                    alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (action: UIAlertAction!) in
                    }))
                    present(alert, animated: true, completion: nil)
                }
                tableView.reloadRows(at: [indexPath], with: .none)   
            }
        }
    }
    
    func reloadRows(at indexPaths: IndexPath, with animation: UITableViewRowAnimation) {
        
    }
    
    override func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        if indexPath.row == 0 {
            return .none
        }
        downLoadVC.delegate = self

        return .delete
    }
    
    override func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "Delete"
    }
    
    //MARK: Handler get proress download
    func getProgress(progress: Float) {
        self.progressDownload = progress
        DispatchQueue.main.async { [weak self] in
            guard let strongSelf = self else{return}
            strongSelf.progressView.progress = (strongSelf.progressDownload!)
        }
    }
    
}

