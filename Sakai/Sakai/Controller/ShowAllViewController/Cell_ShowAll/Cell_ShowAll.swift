//
//  Cell_ShowAll.swift
//  Sakai
//
//  Created by Developer on 12/1/16.
//  Copyright © 2016 Team 4. All rights reserved.
//

import UIKit

class Cell_ShowAll: UITableViewCell {
    //*---------------------------
    @IBOutlet weak var imgShowAllCell: UIImageView!
    @IBOutlet weak var lblTitleChapter: UILabel!
    @IBOutlet weak var lblTeacher: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var imgCheck: UIImageView!
    //*---------------------------
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
