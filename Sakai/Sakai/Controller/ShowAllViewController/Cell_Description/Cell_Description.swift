//
//  Cell_Description.swift
//  Sakai
//
//  Created by Developer on 12/4/16.
//  Copyright © 2016 Team 4. All rights reserved.
//

import UIKit

class Cell_Description: UITableViewCell {
    //*-------------------------
    @IBOutlet weak var lblTeacher: UILabel!
    @IBOutlet weak var lblDateStart: UILabel!
    @IBOutlet weak var txtDescription: UITextView!
    
    //*-------------------------
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
