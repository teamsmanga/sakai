//
//  HomeViewController.swift
//  Sakai
//
//  Created by Developer on 11/23/16.
//  Copyright © 2016 Team 4. All rights reserved.
//

import UIKit
import Firebase
import Alamofire

protocol HomePushDelegate: class {
    func passDataToShowAllView(_ strId:String, _ indxScope:Int)
}

private enum rotateStatus {
    case Portrait
    case Landscape
}
class SearchViewController: UIViewController {
    //*-----------------------------------------
//    let searchController = UISearchController(searchResultsController: nil)
    weak var delegate:HomePushDelegate? = nil
    var isPortrait = true
    var searchController:UISearchController?
    var arrLessons:[Lesson]? = nil
    var arrLessonsFiltered:[Lesson]? = nil
    var arrChapters:[Chapter]? = nil
    var arrChaptersFiltered:[Chapter]? = nil
    var indexScope = 0
    //*-----------------------------------------
    @IBOutlet weak var myTableView: UITableView!
    @IBOutlet weak var mySearchBar: UISearchBar!
    //*-----------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        setStatusBarBackgroundColor(color: orangeSwift) // set background color cho statusBar
        customUISearchBar() // Custom searchBar
        //
        addGestureSwipeToRight()
        //
        mySearchBar.delegate = self
        //
        myTableView.tableHeaderView = UIView()
    }
    
    //
    func addGestureSwipeToRight() {
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.myTableView.addGestureRecognizer(swipeRight)
    }
    
    func respondToSwipeGesture() {
        dismiss(animated: true, completion: nil)
    }
    
    //
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    // add blank space above searchBar when Lanscape
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if isPortrait {
            self.view.addSubview(blankViewLanscape)
        }
        isPortrait = !isPortrait
    }
    
    let blankViewLanscape:UIView = {
        let view = UIView()
        view.frame = CGRect(x: 0, y: 0, width: screenSize.width*2, height: 20.0)
        view.backgroundColor = orangeSwift
        return view
    }()
    // custom UI searchBar
    func customUISearchBar() {
        //*-------------custom SearchBar
        mySearchBar.isTranslucent = false
        searchController = UISearchController(searchResultsController: nil)
         definesPresentationContext = true
        mySearchBar.placeholder = "Type something here ..."
        mySearchBar.tintColor = UIColor.white
        mySearchBar.barTintColor = orangeSwift
        mySearchBar.backgroundColor = orangeSwift
        mySearchBar.backgroundImage = UIImage()
        mySearchBar.backgroundColor = orangeSwift
        
        //*-------------custom textField inside SearchBar
        let textFieldInsideSearchBar = mySearchBar.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.backgroundColor = lessOrangeSwift
        textFieldInsideSearchBar?.textColor = UIColor.white
        let textFieldInsideSearchBarLabel = textFieldInsideSearchBar!.value(forKey: "placeholderLabel") as? UILabel
        textFieldInsideSearchBarLabel?.textColor = UIColor.white
        
        //*-------------custom clear button
        let clearButton = textFieldInsideSearchBar?.value(forKey: "clearButton") as! UIButton
        clearButton.setImage(clearButton.imageView?.image?.withRenderingMode(.alwaysTemplate), for: .normal)
        clearButton.tintColor = UIColor.white
        
        //*--------------custom magnifier glass
        let glassIconView = textFieldInsideSearchBar?.leftView as? UIImageView
        glassIconView?.image = glassIconView?.image?.withRenderingMode(.alwaysTemplate)
        glassIconView?.tintColor = UIColor.white
        //*------------------------
    }
    
    // change background color statusBar
    func setStatusBarBackgroundColor(color: UIColor) {
        
        guard let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else { return }
        
        statusBar.backgroundColor = orangeSwift
    }
    
    // change text in statusBar to lightcontent
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //*--------------------------
    // FUNCTION SEARCH
    fileprivate func filterContentForSearchText(_ searchText : String, scope: Int) {
        let strSearch = mySearchBar.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        if strSearch != "" {
        if scope == 0 {
            arrLessons = Lesson.getLessonsFromSQLite()
            arrLessonsFiltered = arrLessons?.filter() { nil != $0.lesson_name?.lowercased().folding(options: .diacriticInsensitive, locale: .current).range(of: searchText.lowercased().folding(options: .diacriticInsensitive, locale: .current))}
        } else {
            arrChapters = Chapter.getAllChapters()
            arrChaptersFiltered = arrChapters?.filter() { nil != $0.chapter_name?.lowercased().folding(options: .diacriticInsensitive, locale: .current).range(of: searchText.lowercased().folding(options: .diacriticInsensitive, locale: .current))}
            }
        }
    }
}

extension SearchViewController: UISearchBarDelegate {
    //
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        indexScope = selectedScope
        filterContentForSearchText("", scope: indexScope)
    }
    //
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
    }
    //
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filterContentForSearchText((searchBar.text?.trimmingCharacters(in: .whitespacesAndNewlines))!, scope: indexScope)
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
    }
}

extension SearchViewController:UITableViewDataSource {
    //
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    //
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let strSearch = mySearchBar.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        if strSearch == "" {
            return 0
        } else {
            if indexScope == 0 {
                return (arrLessonsFiltered?.count)!
            } else {
                return (arrChaptersFiltered?.count)!
            }
        }
    }
    //
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellId = "cell_search"
        tableView.register(UINib(nibName:"Search_Row", bundle: nil), forCellReuseIdentifier: cellId)
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as? Search_Row
        if let cell = cell {
            if indexScope == 0 {
                cell.textLabel?.text = arrLessonsFiltered?[indexPath.row].lesson_name
                cell.detailTextLabel?.text = arrLessonsFiltered?[indexPath.row].lesson_teacher
                return cell
            } else {
                cell.textLabel?.text = arrChaptersFiltered?[indexPath.row].chapter_name
                cell.detailTextLabel?.text = arrChaptersFiltered?[indexPath.row].chapter_teacher
                return cell
            }
        }
        return UITableViewCell()
        
    }
}

extension SearchViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        dismiss(animated: true, completion: { [weak self] in
            if let delegate = self?.delegate {
                if self?.indexScope == 0 {
                    delegate.passDataToShowAllView((self?.arrLessonsFiltered?[indexPath.row].lesson_id!)!, (self?.indexScope)!)
                } else {
                    delegate.passDataToShowAllView((self?.arrChaptersFiltered?[indexPath.row].chapter_id!)!, (self?.indexScope)!)
                }
            }
        })
    }
}

