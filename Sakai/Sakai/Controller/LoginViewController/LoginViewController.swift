//
//  LoginViewController.swift
//  Sakai
//
//  Created by Thien Lan on 11/23/16.
//  Copyright © 2016 Team 4. All rights reserved.
//

import UIKit
import FirebaseAuth

class LoginViewController: UIViewController {
    
    @IBOutlet weak var btClearPassword: UIButton!
    @IBOutlet weak var btClearEmail: UIButton!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var tfPasswd: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var btnLoginSakai: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tfEmail.delegate = self
        tfPasswd.delegate = self
        //Add tap gesture to scrollview
        scrollView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapGesture)))
        //Add notification for Keyboard
        NotificationCenter.default.addObserver(self, selector: #selector(keyDidAppear(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyDidAppear(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        //Check user login
        if FIRAuth.auth()?.currentUser != nil
        {
            let homeView = HomeViewController()
            homeView.title = "CTC"
            self.navigationController?.pushViewController(homeView, animated: false)
        }
    }
    
    override var prefersStatusBarHidden: Bool{
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        self.view.isUserInteractionEnabled = true
        //Make cornor round
        btnLoginSakai.layer.cornerRadius = 10
        // kiem tra gia tri trong userDefault
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func forgotPassword(_ sender: Any) {
        //Check email textfield is empty
        if tfEmail.text != ""
        {
            FIRAuth.auth()?.sendPasswordReset(withEmail: tfEmail.text!) { [weak self](error) in
                if error == nil
                {
                    //Show alert with single button
                    Helper.shareInstance.showAlertSingleButton(from: self!, title: "CONFIRM!", message: "Please access your email to change password", actions: { })
                }
            }
        }
        else
        {
            //Show alert with single button
            Helper.shareInstance.showAlertSingleButton(from: self, title: "Fail", message: "Your email is empty", actions: {})
        }
    }
    
    // thực hiện login bằng TK sakai
    @IBAction func loginSakai(_ sender: AnyObject) {
        login()
    }
    
    @IBAction func clearEmail(_ sender: Any) {
        tfEmail.text = ""
    }
    
    @IBAction func clearPassword(_ sender: Any) {
        tfPasswd.text = ""
    }
    
    func tapGesture() {
        scrollView.endEditing(true)
    }
    
    func login() {
        self.view.endEditing(false)
        self.view.isUserInteractionEnabled = false
        DispatchQueue.global(qos: .default).async {[weak self] in
            guard let strong = self else { return }
            DispatchQueue.main.async {
                strong.indicator.startAnimating()
            }
            //Check network
            if isConnect != true
            {
                Helper.shareInstance.showAlertSingleButton(from: strong, title: "Connect Fail!", message: "Please check your network.", actions: {
                    strong.indicator.stopAnimating()
                    strong.view.isUserInteractionEnabled = true
                })
            }
            else
            {
                //Login
                FIRAuth.auth()?.signIn(withEmail: strong.tfEmail.text!, password: strong.tfPasswd.text!, completion: { (user, error) in
                    //Login success
                    if error == nil
                    {
                        //Set fake Data to user defaults
                        UserDefaults.standard.setValue(user?.email, forKey: "EmailUser")
                        UserDefaults.standard.setValue(user?.uid, forKey: "IdUser")
                        UserDefaults.standard.setValue("avatar", forKey: "AvatarUser")
                        UserDefaults.standard.setValue("John Doe", forKey: "NameUser")
                        UserDefaults.standard.setValue("+84 8 7300 7300", forKey: "PhoneUser")
                        UserDefaults.standard.setValue("facebook.com/JohnDoe", forKey: "FBUser")
                        //Get user info from firebase
                        DBFirebase.shareInstance.getData()
                        let homeView = HomeViewController()
                        homeView.title = "CTC"
                        strong.navigationController?.pushViewController(homeView, animated: true)
                        strong.indicator.stopAnimating()
                    }
                    //Login fail
                    else
                    {
                        Helper.shareInstance.showAlertSingleButton(from: self!, title: "Login Fail!", message: "Your email or password wrong.", actions: {
                            strong.indicator.stopAnimating()
                            strong.view.isUserInteractionEnabled = true
                        })
                    }
                })
            }
        }
    }
}

//Textfield delegate
extension LoginViewController: UITextFieldDelegate{
    //MARK: Click return on keyboard
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        //Switch to password textfield
        if textField == tfEmail
        {
            tfEmail.resignFirstResponder()
            tfPasswd.text = ""
            tfPasswd.becomeFirstResponder()
        }
        //Password textfield is editting
        else
        {
            login()
        }
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        //Show btClear when textField edditing
        if textField == tfEmail
        {
            btClearEmail.isHidden = false
            btClearPassword.isHidden = true
        }
        else
        {
            btClearEmail.isHidden = true
            btClearPassword.isHidden = false
        }
        return true
    }
}

//Handle Keyboard
extension LoginViewController{
    
    func keyDidAppear(_ notification: NSNotification)
    {
        var userInfo = notification.userInfo!
        let keyboardScreenFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenFrame, from: view.window)
        if notification.name == Notification.Name.UIKeyboardWillHide{
            scrollView.contentOffset = CGPoint.zero
            scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
        else
        {
            if notification.name == Notification.Name.UIKeyboardWillShow
            {
                scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height+20, right: 0)
            }
        }
    }
}
