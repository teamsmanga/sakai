//
//  Home_Cell_Start.swift
//  Sakai_demo
//
//  Created by Developer on 11/28/16.
//  Copyright © 2016 Team 4. All rights reserved.
//

import UIKit

class Home_Cell_Start_End: UICollectionViewCell {

    @IBOutlet weak var lblNumberChapter: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    var arrUIColor = [UIColor]()   // Empty Array of type UIColor
    var indexRow = Int()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //
        self.arrUIColor.append(greenLesson)
        self.arrUIColor.append(yellowLesson)
        self.arrUIColor.append(orangeLesson)
        self.arrUIColor.append(grayBgView)
        //
        self.backgroundColor = arrUIColor[indexRow]
        // Initialization code
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width:0,height: 1.0)
        self.layer.shadowRadius = 2.0
        self.layer.shadowOpacity = 0.6
        self.layer.masksToBounds = false;
    }

}
