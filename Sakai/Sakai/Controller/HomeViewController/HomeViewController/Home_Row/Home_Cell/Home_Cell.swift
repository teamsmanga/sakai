//
//  Home_Cell.swift
//  Sakai
//
//  Created by Developer on 11/23/16.
//  Copyright © 2016 Team 4. All rights reserved.
//

import UIKit

class Home_Cell: UICollectionViewCell {
    
    //*---------------------------------------
    @IBOutlet weak var imageLogo: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTeacher: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var imgCheck: UIImageView!
    //*---------------------------------------
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

        // self.layer.cornerRadius = 5.0
        // self.layer.borderWidth = 1.0
        // self.layer.borderColor = UIColor.clear.cgColor
        // self.layer.masksToBounds = true;
        
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width:1.0,height: 1.0)
        self.layer.shadowRadius = 2.0
        self.layer.shadowOpacity = 0.6
        self.layer.masksToBounds = false
        
    }
}
