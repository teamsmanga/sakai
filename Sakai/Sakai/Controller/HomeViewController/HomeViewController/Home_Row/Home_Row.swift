//
//  Home_Row.swift
//  Sakai
//
//  Created by Developer on 11/28/16.
//  Copyright © 2016 Team 4. All rights reserved.
//

import UIKit

protocol HomeRowDelegate: class {
    //strIdLesson: id của lesson, indexPath: indexpath của cell trong collectionView bên trong tableView
    func passDataToDetailView(_ strIdChapter:String,_ indexpath:IndexPath, _ indexSection:Int)
}

class Home_Row: UITableViewCell {
    //
    var arrChapter = [Chapter]()
    weak var delegate:HomeRowDelegate? = nil
    var currentLesson:Lesson? = nil
    var indexSection:Int? // index để thực hiện chuyển màu cho các row trong tableView bên HomeView
    var dic_idChapter_idLesson = [String:String]()
    var arrAllChaptersOffline:[Chapter]? = nil
    let imageCheck = UIImage(named: "Check")
    var isListChapterOfflineUpdated = false
    //
    var quantityChapter = 0
    //
    @IBOutlet weak var myCollectionView: UICollectionView!
    //
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        if let myLesson = currentLesson {
            arrChapter = Chapter.getChaptersByIdLesson(myLesson.lesson_id!) // lấy các chapter theo idLesson
            //
            myCollectionView.delegate = self
            myCollectionView.dataSource = self
            //
            let layoutCollection_Course = self.myCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
            layoutCollection_Course.scrollDirection = .horizontal
            myCollectionView.showsHorizontalScrollIndicator = false
            //
            quantityChapter = Chapter.getChaptersByIdLesson((currentLesson?.lesson_id)!).count
        }
        
        // tạo dic từ idchapter và lesson_chapter_id để kiểm tra record có trong table offline
        arrAllChaptersOffline = Chapter.getAllChaptersOffline()
        for item in arrAllChaptersOffline! {
            dic_idChapter_idLesson[item.chapter_id!] = item.chapter_lesson_id
        }
        // add observer để reload table khi thực hiện save và delete tại homeSubView và showAllView
        NotificationCenter.default.addObserver(forName: NSNotification.Name("notiUpdateDataOffline"), object: nil, queue: nil, using: handleNotitfication)
    }
    
    // thực hiện reload tableView khi nhận được notifi
    func handleNotitfication(notification: Notification) {
        isListChapterOfflineUpdated = true
        myCollectionView.reloadData()
        // tạo cell các chapter trong lesson
        arrAllChaptersOffline = Chapter.getAllChaptersOffline()
        // tạo dic từ idchapter và lesson_chapter_id để kiểm tra record có trong table offline
        dic_idChapter_idLesson.removeAll()
        for item in arrAllChaptersOffline! {
            dic_idChapter_idLesson[item.chapter_id!] = item.chapter_lesson_id!
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("notiUpdateDataOffline"), object: nil)
    }
   
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}

extension Home_Row:UICollectionViewDataSource, UICollectionViewDelegate {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrChapter.count+2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.row {
        case 0:
            let cellId = "Home_Cell_Start_End"
            myCollectionView.register(UINib(nibName:cellId, bundle: nil), forCellWithReuseIdentifier: cellId)
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as? Home_Cell_Start_End
            cell?.lblTitle.text = String(format: "📚 %@", (currentLesson?.lesson_name)!)
            cell?.lblNumberChapter.text = "\(quantityChapter) Lesson"
            cell?.indexRow = indexPath.section
            cell?.contentView.backgroundColor = arrUIColor[indexSection!]
            cell?.awakeFromNib()
            //
            return cell!
        case arrChapter.count + 1:
            let cellId = "Home_Cell_Start_End"
            myCollectionView.register(UINib(nibName:cellId, bundle: nil), forCellWithReuseIdentifier: cellId)
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as? Home_Cell_Start_End
            cell?.contentView.backgroundColor = arrUIColor[indexSection!]
            cell?.lblTitle.text = "📖 See All"
            cell?.lblNumberChapter.text = "\(quantityChapter) Lesson"
            cell?.indexRow = indexPath.section
            cell?.awakeFromNib()
            //
            return cell!
        default:
            let cellId = "Home_Cell"
            myCollectionView.register(UINib(nibName:cellId, bundle: nil), forCellWithReuseIdentifier: cellId)
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as? Home_Cell
            //
            let currChapter = arrChapter[indexPath.row-1]
            cell?.lblTitle.text = String(format: "📙 %@", currChapter.chapter_name!)
            cell?.lblTeacher.text = String(format: "🙍🏻‍♂️ %@", currChapter.chapter_teacher!)
            //
            let usDateFormat = DateFormatter()
            usDateFormat.dateFormat = "d/MM/y"
            usDateFormat.locale = NSLocale(localeIdentifier: "vi_VN") as Locale!
            cell?.lblDate.text = ""

            // check if value of key chapter_id not nil to make sure the chapter is offline and show icon "Check"
            if (arrAllChaptersOffline?.count)! > 0 {
                isListChapterOfflineUpdated = !isListChapterOfflineUpdated
                if let lesson_chapter_id = dic_idChapter_idLesson[(arrChapter[indexPath.row-1].chapter_id!)] {
                    if lesson_chapter_id == currChapter.chapter_lesson_id! {
                        cell?.imgCheck.image = imageCheck
                    } else {
                        cell?.imgCheck.image = nil
                    }
                } else {
                    cell?.imgCheck.image = nil
                }
            } else {
                cell?.imgCheck.image = nil
            }
            //
            DispatchQueue.global().async {
                if (currChapter.chapter_logoPath?.characters.count)! > 1 {
                    let imgLogo = getImgFromUrl(URL(string: "http://gst.fsoft.com.vn/Elearning-App/" + currChapter.chapter_logoPath!)!)
                    DispatchQueue.main.async {
                        cell?.imageLogo.image = imgLogo
                    }
                }
            }
            return cell!
        }
    }
    //
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if delegate != nil {
            if indexPath.row <= arrChapter.count && indexPath.row > 0 {
                let strIdChapter = String(format: "%@", (arrChapter[indexPath.row-1].chapter_id)!)
                delegate?.passDataToDetailView(strIdChapter, indexPath, indexSection!)
            } else {
                delegate?.passDataToDetailView("", indexPath, indexSection!)
            }
        }
    }
}

extension Home_Row:UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 130, height: 140)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0,left: 0,bottom: 0,right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
}

