//
//  HomeViewController.swift
//  Sakai
//
//  Created by Developer on 11/23/16.
//  Copyright © 2016 Team 4. All rights reserved.
//
import UIKit
import Firebase
import FBSDKLoginKit
import Alamofire

class HomeViewController: UIViewController {
    //*-----------------------------------------
    var isPortrait = true
    var searchController:UISearchController?
    var arrLessons = [Lesson]()
    let home = homeSubViewViewController()
    //*-----------------------------------------
    @IBOutlet weak var myTableView: UITableView!
    @IBOutlet weak var segOfflineOnline: UISegmentedControl!
    @IBOutlet weak var myTabbar: UITabBar!
    @IBOutlet weak var myTabbarItem_1: UITabBarItem!
    @IBOutlet weak var myTabbarItem_2: UITabBarItem!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var myIndicator: UIActivityIndicatorView!
    
    //
    @IBAction func sementOn_Off(_ sender: UISegmentedControl) {
        let homesubview = home.view
        //
        let frameSubView = self.myTableView.frame
        if sender.selectedSegmentIndex == 1 {
            homesubview?.frame = CGRect(x: 0, y: frameSubView.origin.y, width: frameSubView.size.width, height: frameSubView.size.height-2)
            self.view.addSubview(homesubview!)
            UIView.animate(withDuration: 0.3 ) {
                homesubview?.alpha = 1
                self.addChildViewController(self.home)
            }
//            self.addChildViewController(self.home)
            myTabbar.selectedItem = myTabbarItem_1
        } else {
            myTabbar.selectedItem = myTabbarItem_1
            UIView.animate(withDuration: 0.3, animations: {
                homesubview?.alpha = 0
            } , completion: { (finished: Bool) in
                if(finished)
                {
                    self.home.view.removeFromSuperview()
                    self.home.removeFromParentViewController()
                }
            })
        }
    }
    
    //*-----------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        //
        print(getScoresFromFirebase())
        
        //
        self.navigationController?.isNavigationBarHidden = false
        //
        myTabbar.selectedItem = myTabbarItem_1
        //
        myTableView.tableFooterView = UIView()
        segOfflineOnline.tintColor =  orangeSwift
        //
        addItemsNavigation()
        //
        self.view.addSubview(lineSeparatorView)
        // add observer load lai table khi load xong data từ server xuống sqlite
        NotificationCenter.default.addObserver(forName: NSNotification.Name("notiFinishLoadData"), object: nil, queue: nil) { notification in
            self.arrLessons = notification.userInfo?["data"] as! [Lesson]
            self.myIndicator.stopAnimating()
            self.myIndicator.isHidden = true
            if self.arrLessons.count == 0   {
                let alertController = UIAlertController(title: "Messages", message: "Can not connect to server\n Please try again later", preferredStyle: .alert)
                let closeAction = UIAlertAction(title: "Close", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                    
                }
                alertController.addAction(closeAction)
                self.present(alertController, animated: true, completion: nil)
            }
            DispatchQueue.main.async {
                self.myTableView.reloadData()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        if isConnect == false {
//            let alertController = UIAlertController(title: "Messages", message: "Can not connect to server\n Please check your network", preferredStyle: .alert)
//            let closeAction = UIAlertAction(title: "Close", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
//                self.myIndicator.stopAnimating()
//                self.myIndicator.isHidden = true
//                return 
//            }
//            alertController.addAction(closeAction)
//            self.present(alertController, animated: true, completion: nil)
//        }
        //
        myIndicator.startAnimating()
        //
         _ = dataManager.defaultDataManager()
        
        if arrLessons.count == 0 {
            arrLessons = Lesson.getLessonsFromSQLite()
        }
    }
    
    // add blank space above searchBar when Lanscape
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    // *----------------------------------define blank space
    let blankViewLanscape:UIView = {
        let view = UIView()
        view.frame = CGRect(x: 0, y: 0, width: screenSize.width*2, height: 20.0)
        view.backgroundColor = orangeSwift
        return view
    }()
    
    let lineSeparatorView: UIView = {
        let view = UIView()
        view.backgroundColor = orangeSwift
        view.frame = CGRect(x: 0, y: screenSize.height-115, width: screenSize.width, height: 2)
        return view
    }()
    
    //MARK -----------lay diem tren firebase cua bai thi---------------------------------------------------
    func getScoresFromFirebase()->[[String:AnyObject]] {
        var array_dictionary_score = [[String:AnyObject]()]
        let array_lessson = Lesson.getLessonsFromSQLite()
        for curLesson in array_lessson {
            let array_chapter = Chapter.getAllChapters()
            for curChapter in array_chapter {
                DBFirebase.shareInstance.getGrade(idCourse: curLesson.lesson_id!, idLesson: curChapter.chapter_id!, completeHandle: {[weak self](grade)  in
                    
                        let newDic = ["idCourse": curLesson.lesson_id,
                                      "idLesson": curChapter.chapter_id,
                                      "point": String(format:"%@",grade)]
                        array_dictionary_score.append(newDic as [String : AnyObject])
                })
            }
        }
        return array_dictionary_score
    }
    //
    func addItemsNavigation() {
        //
        let leftBtn = UIButton()
        leftBtn.setImage(UIImage(named: "Search"), for: .normal)
        leftBtn.frame.size = CGSize(width: 20, height: 20)
        leftBtn.addTarget(self, action: #selector(showSearchView), for: .touchUpInside)
        let item1 = UIBarButtonItem()
        item1.customView = leftBtn
        //
        let rightBtn = UIButton()
        rightBtn.setImage(UIImage(named: "Menu"), for: .normal)
        rightBtn.frame.size = CGSize(width: 20, height: 20)
        rightBtn.addTarget(self, action: #selector(showSettingView), for: .touchUpInside)
        let item2 = UIBarButtonItem()
        item2.customView = rightBtn
        //
        self.navigationItem.leftBarButtonItem = item1
        self.navigationItem.rightBarButtonItem = item2
        
    }
    
    // show search view
    func showSearchView() {
        let searchView = SearchViewController()
        searchView.modalTransitionStyle = .coverVertical
        searchView.delegate = self
        self.present(searchView, animated: true, completion: { _ in })
        
//        let downloadView = DownloadViewController()
//        self.navigationController?.pushViewController(downloadView, animated: true)
    }
    
    // show setting view
    func showSettingView() {
        let alertController = UIAlertController(title: "Messages", message: "What type of option do you want?", preferredStyle: .actionSheet)
        // action logout
        let logoutAction = UIAlertAction(title: "Logout", style: UIAlertActionStyle.destructive) { (result : UIAlertAction) -> Void in
            let firebaseAuth = FIRAuth.auth()
            do {
                try firebaseAuth?.signOut()
                FBSDKLoginManager().logOut()
                _  = self.navigationController?.popToRootViewController(animated: true)
            } catch {}
//            _ = self.navigationController?.popToRootViewController(animated: true)
            //
            UserDefaults.standard.setValue("", forKey: "EmailUser")
            UserDefaults.standard.setValue("", forKey: "idUser")
            UserDefaults.standard.setValue("", forKey: "AvatarUser")
            UserDefaults.standard.setValue("", forKey: "NameUser")
            UserDefaults.standard.setValue("", forKey: "PhoneUser")
            UserDefaults.standard.setValue("", forKey: "FBUser")
        }
        
        let profileAction = UIAlertAction(title: "Profile", style: UIAlertActionStyle.default) { [weak self](result : UIAlertAction) -> Void in
            self?.navigationController?.pushViewController(AccountViewController(), animated: true)
        }
        
        //
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
        alertController.addAction(logoutAction)
        alertController.addAction(profileAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    //
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //
    deinit {
        print("deinit home view")
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("notiFinishLoadData"), object: nil)
    }
}

extension HomeViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrLessons.count > 0 {
            self.myIndicator.stopAnimating()
            self.myIndicator.isHidden = true
        }
        return arrLessons.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellId = "cell"
        myTableView.register(UINib(nibName:"Home_Row", bundle:nil), forCellReuseIdentifier: cellId)
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as? Home_Row
        //
        if let myCell = cell {
            //
            myCell.delegate = self
            myCell.selectionStyle = .none
            myCell.currentLesson = arrLessons[indexPath.row]
            myCell.indexSection = indexPath.row
            myCell.awakeFromNib()
            return myCell
        }
        return UITableViewCell()
    }
}

extension HomeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    //
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 170.0
    }
}

extension HomeViewController:UITabBarDelegate {
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        let homesubview = home.view
        //
        let frameSubView = self.myTableView.frame
        //
        let arrTitleTabar = ["Courses", "Scores"]
        if item.title == arrTitleTabar[1] { // select the second tab (Scores)
            homesubview?.frame = CGRect(x: 0, y: frameSubView.origin.y, width: frameSubView.size.width, height: frameSubView.size.height-2)
            self.view.addSubview(homesubview!)
            UIView.animate(withDuration: 0.3 ) {
                homesubview?.alpha = 1
                self.addChildViewController(self.home)
            }
            self.addChildViewController(self.home)
            myTabbarItem_2.badgeColor = orangeSwift
            myTabbar.selectedItem = myTabbarItem_2
            
        } else { // select the first tab (Courses)
            UIView.animate(withDuration: 0.3, animations: {
                homesubview?.alpha = 0
            } , completion: { (finished: Bool) in
                if(finished) {
                    self.home.view.removeFromSuperview()
                    self.home.removeFromParentViewController()
                    self.myTabbar.selectedItem = self.myTabbarItem_1
                    self.myTabbar.selectedItem?.badgeColor = orangeSwift
                    self.segOfflineOnline.selectedSegmentIndex = 0
                }
            })
        }
    }
}

extension HomeViewController: HomeRowDelegate {
    // Data: string mã id của chapter
    // indexSection:Int
    // indexPath là của cell trong collectionView
    func passDataToDetailView(_ strIdChapter: String, _ indexPath: IndexPath, _ indexSection:Int) {
        let numberCellInRow = Chapter.getChaptersByIdLesson(arrLessons[indexSection].lesson_id!).count // số cell dang co trong mỗi row
        if indexPath.row != 0 && indexPath.row != (numberCellInRow+1) {
            let story = UIStoryboard(name: "Main", bundle: nil)
            let lessonVC = story.instantiateViewController(withIdentifier: "lessonViewController") as? LessonViewController
            lessonVC?.idChapter = strIdChapter
            self.navigationController?.pushViewController(lessonVC!, animated: true)
        } else {
            let showAllView = ShowAllTableViewController()
            showAllView.idLesson = arrLessons[indexSection].lesson_id
            showAllView.title = arrLessons[indexSection].lesson_name
            self.navigationController?.pushViewController(showAllView, animated: true)
        }
    }
}

extension HomeViewController: HomePushDelegate {
    func passDataToShowAllView(_ strId:String, _ indxScope:Int) {
        // indxScope để kiểm tra đang search lesson or chapter để push view
        // strId: String là id của lesson hoặc chapter được truyền từ searchView
        if indxScope == 0 {
            let showAllView = ShowAllTableViewController()
            showAllView.idLesson = strId
            showAllView.title = Lesson.getLessonByIdLesson(strId).lesson_name
            self.navigationController?.pushViewController(showAllView, animated: true)
        }
        
    }
}

