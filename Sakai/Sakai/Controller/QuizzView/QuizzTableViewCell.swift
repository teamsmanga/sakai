//
//  QuizzTableViewCell.swift
//  Sakai
//
//  Created by Thien Lan on 11/28/16.
//  Copyright © 2016 Team 4. All rights reserved.
//

import UIKit

class QuizzTableViewCell: UITableViewCell {
    
    @IBOutlet weak var btCheck: CheckBox!
    @IBOutlet weak var lbAnswer: UILabel!
    var key:String?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
        if selected
        {
            btCheck.isChecked = true
        }
        else
        {
            btCheck.isChecked = false
        }
    }
}
