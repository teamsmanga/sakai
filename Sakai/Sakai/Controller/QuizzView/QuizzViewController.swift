//
//  QuizzViewController.swift
//  Sakai
//
//  Created by Thien Lan on 11/25/16.
//  Copyright © 2016 Team 4. All rights reserved.
//

import UIKit

class QuizzViewController: UIViewController {
    
    @IBOutlet weak var constrainBtSubmit: NSLayoutConstraint!
    @IBOutlet weak var btNext: UIButton!
    @IBOutlet weak var lbnumQuestion: UILabel!
    @IBOutlet weak var lbTime: UILabel!
    @IBOutlet weak var tfQuestion: UITextView!
    @IBOutlet weak var btSubmit: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btPrevious: UIButton!
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var viewQuestion: UIView!
    
    var question = Question()
    var arrayAnswer:[String] = []
    var arrayKey:[String] = []
    var currentQuestion = 0
    var seletedAnswer:[[String]] = []
    var totalTime:Int = 10
    var doQuizzTime = 0
    var idChapter:String! = String()
    var idCourse:String! = String()
    var customQueue = DispatchQueue(label: "MyQueue", qos: .default, attributes: DispatchQueue.Attributes.concurrent)
    var isSubmitted = false
    var numTrue:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Register cell
        tableView.register(UINib(nibName: "QuizzTableViewCell", bundle: nil), forCellReuseIdentifier: "QuizzCell")
        //Make progress view run
        timeCountDown()
        //Get data of Quiz from server
        
            ConnectServer.shareInstance.getData(url: linkAssignment) {[weak self](respone) in
                guard let strong = self else { return }
                //Set data to Quizz
                ConnectServer.shareInstance.GetQuizzApi(respone)
                strong.question = (GetQuizz.getQuizz.listQuestion?[strong.currentQuestion])!
                //Get answer text
                strong.parseAnswer(a: strong.question.answer!)
                DispatchQueue.main.async {
                    strong.navigationItem.title = GetQuizz.getQuizz.title
                    strong.tfQuestion.text = strong.question.question
                    strong.tableView.reloadData()
                }
                //Create list selectedAnswer for each question
                for _ in 0..<(GetQuizz.getQuizz.listQuestion?.count)!
                {
                    strong.seletedAnswer.append([String]())
                }
                if strong.seletedAnswer.count == 1
                {
                    strong.btSubmit.isHidden = false
                    strong.btNext.isEnabled = false
                }
            }
    }
    
    //Get list answer from Dictionary listAnswer
    func parseAnswer(a: [String:String]) {
        arrayAnswer.removeAll()
        arrayKey.removeAll()
        for (key,value) in a
        {
            arrayAnswer.append(value)
            arrayKey.append(key)
        }
        tableView.reloadData()
    }
    
    @IBAction func clickSubmit(_ sender: AnyObject) {
        //Create barrier
        let barrier = DispatchWorkItem(qos: .default, flags: .barrier, block: {
            sleep(30)
        })
        customQueue.async(execute: barrier)
        switchScreen()
    }
    
    @IBAction func clickNext(_ sender: AnyObject) {
        if arrayAnswer.count > 0
        {
            UIView.transition(with: viewQuestion, duration: 0.5, options: UIViewAnimationOptions.transitionFlipFromRight, animations: nil, completion: { [weak self](bool) in
                self?.tableView.reloadData()
            })
            currentQuestion += 1
            question = (GetQuizz.getQuizz.listQuestion?[currentQuestion])!
            parseAnswer(a: question.answer!)
            lbnumQuestion.text = "No: \(currentQuestion + 1)"
            tfQuestion.text = question.question
            btPrevious.isEnabled = true
            //Check condition to enable btNext
            if currentQuestion == (GetQuizz.getQuizz.listQuestion?.count)! - 1
            {
                btNext.isEnabled = false
                btSubmit.isHidden = false
            }
        }
        else
        {
            btNext.isEnabled = false
            btSubmit.isEnabled = false
        }
    }
    
    @IBAction func clickPrevious(_ sender: AnyObject) {
        if arrayAnswer.count > 0
        {
            UIView.transition(with: viewQuestion, duration: 0.5, options: UIViewAnimationOptions.transitionFlipFromRight, animations: nil, completion: { [weak self](bool) in
                self?.tableView.reloadData()
            })
            currentQuestion -= 1
            question = (GetQuizz.getQuizz.listQuestion?[currentQuestion])!
            parseAnswer(a: question.answer!)
            lbnumQuestion.text = "No: \(currentQuestion + 1)"
            tfQuestion.text = question.question
            btSubmit.isHidden = true
            btNext.isEnabled = true
            //Condition to enable btPrevious
            if currentQuestion == 0
            {
                btPrevious.isEnabled = false
            }
        }
        else
        {
            btPrevious.isEnabled = false
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tableView.layer.borderWidth = 3
        tableView.layer.borderColor = UIColor.lightGray.cgColor
        tableView.layer.cornerRadius = 10
        lbnumQuestion.text = "No: \(currentQuestion + 1)"
        //Change height of progress bar
        progressBar.transform = CGAffineTransform(scaleX: 1, y: 3)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func timeCountDown() {
        customQueue.async{[weak self] in
            guard let strong = self else { return }
            strong.lbTime.text = String(format: "%02d:%02d", (strong.totalTime / 60) % 60, strong.totalTime % 60)
            //Update progress bar
            while strong.doQuizzTime < strong.totalTime
            {
                sleep(1)
                DispatchQueue.main.async {
                    strong.lbTime.text = String(format: "%02d:%02d",
                                                (((strong.totalTime - strong.doQuizzTime) / 60) % 60),
                                                (strong.totalTime - strong.doQuizzTime) % 60)
                    strong.progressBar.setProgress(Float(Float(strong.doQuizzTime) / Float(strong.totalTime)), animated: true)
                }
                strong.doQuizzTime += 1
            }
            //Check quiz submitted
            if strong.isSubmitted == false
            {
                Helper.shareInstance.showAlertSingleButton(from: strong, title: "Time Out!", message: "Time is over", actions:{
                    strong.switchScreen()
                })
            }
        }
    }
    
    func switchScreen() {
        isSubmitted = true
        //Insert quiz result to firebase
        DBFirebase.shareInstance.writeData(idCourse: idCourse, idLesson: idChapter, point: getScore())
        let vc = QuizzResultViewController()
        vc.doQuizzTime = doQuizzTime
        vc.totalTime = totalTime
        vc.score = getScore()
        vc.totalTime = totalTime
        vc.numQuestion = (GetQuizz.getQuizz.listQuestion?.count)!
        vc.trueQuestions = numTrue
        var totalScore = 0
        if GetQuizz.getQuizz.listQuestion != nil
        {
            for i in GetQuizz.getQuizz.listQuestion!
            {
                totalScore += i.score!
            }
        }
        vc.totalScore = totalScore
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    private func getScore() -> Int {
        var correctAnswer = ""
        var totalScore = 0
        for i in 0..<currentQuestion
        {
            correctAnswer = (GetQuizz.getQuizz.listQuestion?[i].correctAnswer!)!
            var check = false
            if GetQuizz.getQuizz.listQuestion?[i].questionType == "Multiple Correct"
            {
                //Check num of selected answers
                if seletedAnswer[i].count >= 2
                {
                    for j in seletedAnswer[i]
                    {
                        //Check selected answer is in correct answer
                        if correctAnswer.contains(j)
                        {
                            check = true
                        }
                        else
                        {
                            check = false
                            break
                        }
                    }
                }
            }
            if check == true
            {
                totalScore += (GetQuizz.getQuizz.listQuestion?[i].score!)!
                numTrue += 1
            }
        }
        return totalScore
    }
}

//TableView Delegate and DataSource
extension QuizzViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayAnswer.count
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        //Selected answer was checked
        if seletedAnswer[currentQuestion].contains((cell as! QuizzTableViewCell).key!)
        {
            cell.isSelected = true
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "QuizzCell", for: indexPath) as! QuizzTableViewCell
        cell.lbAnswer.text = arrayAnswer[indexPath.row]
        cell.key = arrayKey[indexPath.row]
        return cell
    }
    
    //Deselect cell
    func tableView(_ tableView: UITableView, willDeselectRowAt indexPath: IndexPath) -> IndexPath? {
        let cell = tableView.cellForRow(at: indexPath) as! QuizzTableViewCell
        var selected = seletedAnswer[currentQuestion]
        for i in 0..<selected.count
        {
            //If cell is selected, remove it
            if cell.key == selected[i]
            {
                selected.remove(at: i)
                break
            }
        }
        seletedAnswer[currentQuestion] = selected
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! QuizzTableViewCell
        seletedAnswer[currentQuestion].append(cell.key!)
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}
