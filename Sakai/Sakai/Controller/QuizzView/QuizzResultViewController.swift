//
//  QuizzResultViewController.swift
//  Sakai
//
//  Created by Thien Lan on 12/4/16.
//  Copyright © 2016 Team 4. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FBSDKShareKit

class QuizzResultViewController: UIViewController {
    
    @IBOutlet weak var lbLessonName: UILabel!
    @IBOutlet weak var lbCourseName: UILabel!
    @IBOutlet weak var lbTrueAnswer: UILabel!
    @IBOutlet weak var lbTime: UILabel!
    @IBOutlet weak var lbScore: UILabel!
    @IBOutlet weak var progressTrueAnswer: UIProgressView!
    @IBOutlet weak var progressTime: UIProgressView!
    @IBOutlet weak var progressScore: UIProgressView!
    
    var doQuizzTime:Int = 0
    var totalTime:Int = 0
    var numQuestion:Int = 0
    var trueQuestions:Int = 0
    var score:Int = 0
    var totalScore:Int = 0
    var courseName:String?
    var lessonName:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "RESULT"
        //Resize image
        let image:UIImage = Helper.shareInstance.resizeImage(image: #imageLiteral(resourceName: "share_icon"), size: CGSize(width: 20, height: 20))
        let imageHome:UIImage = Helper.shareInstance.resizeImage(image: #imageLiteral(resourceName: "home"), size: CGSize(width: 20, height: 20))
        //Set left and right button on navigation bar
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(shareToFace))
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: imageHome, style: .plain, target: self, action: #selector(backToHome))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //Change height of progress bar
        progressTrueAnswer.transform = CGAffineTransform(scaleX: 1, y: 3)
        progressTrueAnswer.layer.cornerRadius = 3
        progressTime.transform = CGAffineTransform(scaleX: 1, y: 3)
        progressTime.layer.cornerRadius = 3
        progressScore.transform = CGAffineTransform(scaleX: 1, y: 3)
        progressScore.layer.cornerRadius = 3
        //Chang process of progress bar
        if totalTime == 0
        {
            progressTime.progress = 1
        }
        else
        {
            progressTime.progress = Float(doQuizzTime)/Float(totalTime)
        }
        
        if numQuestion == 0
        {
            progressTrueAnswer.progress = 1
        }
        else
        {
            progressTrueAnswer.progress = Float(trueQuestions)/Float(numQuestion)
        }
        
        if totalScore == 0
        {
            progressScore.progress = 1
        }
        else
        {
            progressScore.progress = Float(score)/Float(totalScore)
        }
        lbTime.text = String(format: "%02d:%02d/%02d:%02d",
                             (doQuizzTime / 60) % 60, doQuizzTime % 60,
                             (totalTime / 60) % 60, totalTime % 60)
        lbScore.text = "\(score)/\(totalScore)"
        lbTrueAnswer.text = "\(trueQuestions)/\(numQuestion)"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func shareToFace() {
        //Check user have login to facebook
        if FBSDKAccessToken.current() == nil
        {
            //Login to to facebook
            let manager = FBSDKLoginManager()
            manager.logIn(withPublishPermissions: ["publish_actions"], from: self, handler: { [weak self](result, error) in
                if error == nil && !(result?.isCancelled)!
                {
                    self?.showShareDialog((self?.view)!)
                }
            })
        }
        else
        {
            showShareDialog(self.view)
        }
    }
    
    func backToHome()
    {
        let allVC = self.navigationController?.viewControllers
        if  let inventoryListVC = allVC?[1] as? HomeViewController {
            self.navigationController!.popToViewController(inventoryListVC, animated: true)
        }
    }
    
    func showShareDialog(_ view: UIView) {
        //Take screenshot
        let photo = Helper.shareInstance.screenShot(screen: self.view)
        //Set share content
        let sharePhoto = FBSDKSharePhoto()
        sharePhoto.image = photo
        sharePhoto.isUserGenerated = true
        let content = FBSDKSharePhotoContent()
        content.photos = [sharePhoto]
        //Show share dialog
        let diaglog = FBSDKShareDialog()
        diaglog.shareContent = content
        diaglog.show()
    }
}
