//
//  AccountViewController.swift
//  Sakai
//
//  Created by Thien Lan on 11/23/16.
//  Copyright © 2016 Team 4. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FirebaseAuth

class AccountViewController: UIViewController {
    
    @IBOutlet weak var ivAccount: UIImageView!
    @IBOutlet weak var viewProfile: UIView!
    @IBOutlet weak var viewScheduler: UIView!
    @IBOutlet weak var viewAssignment: UIView!
    @IBOutlet weak var viewQuizz: UIView!
    @IBOutlet weak var lbUserName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Set title navigator bar
        navigationItem.title = "ACCOUNT"
        lbUserName.text = GetUser.get?.name
        //Add tap gesture
        viewScheduler.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(switchScreen(_:))))
        viewProfile.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(switchScreen(_:))))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //Border view
        ivAccount.layer.borderWidth = 2
        ivAccount.layer.borderColor = UIColor.lightGray.cgColor
        ivAccount.layer.cornerRadius = ivAccount.frame.size.height / 2
        boderView(view: viewProfile)
        boderView(view: viewScheduler)
        boderView(view: viewAssignment)
        boderView(view: viewQuizz)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func clickLogout(_ sender: Any) {
        let firebaseAuth = FIRAuth.auth()
        do {
            //Logout Firebase
            try firebaseAuth?.signOut()
            //Logout Facebook
            FBSDKLoginManager().logOut()
            _  = self.navigationController?.popToRootViewController(animated: true)
        } catch {}
    }
    
    func switchScreen(_ sender:UITapGestureRecognizer) {
        switch (sender.view?.tag)! {
        case 1:
            self.present(ProfileViewController(), animated: true, completion: nil)
        case 2:
            self.navigationController?.pushViewController(MySchedulerViewController(), animated: true)
            //        case 3:
            //            self.navigationController?.pushViewController(ProfileViewController(), animated: true)
            //        case 4:
        //            self.navigationController?.pushViewController(ProfileViewController(), animated: true)
        default:
            break
        }
    }
    
    private func boderView(view: UIView){
        view.layer.borderWidth = 3
        view.layer.borderColor = UIColor.darkGray.cgColor
        view.layer.cornerRadius = view.frame.size.height / 2
    }
}
