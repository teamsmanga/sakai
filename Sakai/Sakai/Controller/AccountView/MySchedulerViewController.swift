//
//  MySchedulerViewController.swift
//  Sakai
//
//  Created by Thien Lan on 12/6/16.
//  Copyright © 2016 Team 4. All rights reserved.
//

import UIKit
import Alamofire
import FirebaseStorage

class MySchedulerViewController: UIViewController {
    
    @IBOutlet weak var webView: UIWebView!
    var storage:FIRStorage? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        storage = FIRStorage.storage()
        //Create reference to storage firebase
        let storageRef = storage?.reference(forURL: "gs://sakai-5974f.appspot.com/")
        //Reference path to file  in firebase
        let clc = storageRef?.child("clc.html")
        //Create path to file in local
        let documentDir = NSSearchPathForDirectoriesInDomains(.applicationDirectory, .userDomainMask, true)[0] as NSString
        let filePatch = documentDir.appendingPathComponent("clc.html")
        // Download to the local filesystem
        let downloadTask = clc?.write(toFile: URL(fileURLWithPath: filePatch), completion: { (url, error) in
            if error == nil
            {
                self.webView.loadRequest(URLRequest(url: url!))
            }
        })
        downloadTask?.resume()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
