//
//  TestNoti.swift
//  Sakai
//
//  Created by Thien Lan on 12/1/16.
//  Copyright © 2016 Team 4. All rights reserved.
//

import Foundation
import UIKit
import UserNotifications

class TestNoti{
    private init(){}
    static var shareInstance = TestNoti()
    
    func registerNotification() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .sound, .alert]) { (bool, error) in
        }
    }
    
    func pushNotification(title:String, body: String) {
        let content = UNMutableNotificationContent()
        content.title = title
        content.body = body
        content.sound = UNNotificationSound.default()
//        content.badge = 
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1.0, repeats: false)
        let request = UNNotificationRequest(identifier: "download", content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request) { (error) in
        }
    }
    
}
