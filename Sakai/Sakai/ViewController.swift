//
//  ViewController.swift
//  Sakai
//
//  Created by dang hung on 11/22/16.
//  Copyright © 2016 Team 4. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func dwew(_ sender: AnyObject) {
        self.navigationController?.pushViewController(AccountViewController(), animated: true)
    }

}

