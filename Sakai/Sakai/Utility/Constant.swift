//
//  Constant.swift
//  Sakai
//
//  Created by Developer on 11/23/16.
//  Copyright © 2016 Team 4. All rights reserved.
//

import Foundation
import UIKit

let screenSize = UIScreen.main.bounds
var isConnect = false
let nameFileSqlite = "Sakai.sqlite"
let urlAPIGetAllCourse = "http://gst.fsoft.com.vn/Elearning-App/api/node/findAllNodeParrent"
let urlAPIGetDataById = "http://gst.fsoft.com.vn/Elearning-App/user/findOneById/584922852d0e2ea6a15068cc"
let linkAssignment = "https://firebasestorage.googleapis.com/v0/b/sakai-5974f.appspot.com/o/document?alt=media&token=c452ff4a-48a6-4398-8bab-4cb1b9720399"

var  isDownloaded : Bool?

// lấy image từ url
func getImgFromUrl(_ url:URL)->UIImage {
//    let url = URL(string: "http://gst.fsoft.com.vn/Elearning-App/resources/logo/sakai.jpg")
    let data = try? Data(contentsOf: url)
    if let dataImg = data {
        let imgLogoPath = UIImage(data: dataImg)
        return imgLogoPath!
    }
    let uiimg = #imageLiteral(resourceName: "sakai")
    return uiimg
}
