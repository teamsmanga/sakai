//
//  Helper.swift
//  Sakai
//
//  Created by Thien Lan on 11/24/16.
//  Copyright © 2016 Team 4. All rights reserved.
//

import Foundation
import UIKit

class Helper{
    
    private init(){}
    
    static var shareInstance = Helper()
    
    func showAlertTwoButton(from view:UIViewController, title:String, message:String, actions:@escaping ()->Void) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "NO", style: UIAlertActionStyle.cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "YES", style: UIAlertActionStyle.default, handler: { (action) in
            actions()
            alert.dismiss(animated: true, completion: nil)
        }))
        view.present(alert, animated: true, completion: nil)
    }
    
    func showAlertSingleButton(from view:UIViewController, title:String, message:String, actions:@escaping ()->Void) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "YES", style: UIAlertActionStyle.default, handler: { (action) in
            actions()
            alert.dismiss(animated: true, completion: nil)
        }))
        view.present(alert, animated: true, completion: nil)
    }
    
    func loadDataWithName(name: String = "scheduler.txt") -> Data?{
        let documentDir = NSSearchPathForDirectoriesInDomains(.applicationDirectory, .userDomainMask, true)[0] as NSString
        let filePatch = documentDir.appendingPathComponent(name)
        
        let data = NSData(contentsOfFile: filePatch)
        return data as Data?
    }
    
    func screenShot(screen: UIView) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(screen.bounds.size, false, UIScreen.main.scale)
        screen.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
    func resizeImage(image: UIImage, size: CGSize) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
}
