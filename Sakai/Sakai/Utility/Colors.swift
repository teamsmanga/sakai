//
//  File.swift
//  Sakai
//
//  Created by Developer on 11/23/16.
//  Copyright © 2016 Team 4. All rights reserved.
//

import Foundation
import UIKit

//let orangeSwift = UIColor(colorLiteralRed: 249/255, green: 74/255, blue: 58/255, alpha: 1)
let orangeSwift = UIColor(red: 249/255, green: 74/255, blue: 58/255, alpha: 1.0)
let lessOrangeSwift = UIColor(red: 249/255, green: 144/255, blue: 135/255, alpha: 1.0)
let grayBgView = UIColor(red: 237/255, green: 237/255, blue: 237/255, alpha: 1.0)
let greenLesson = UIColor(red: 174/255, green: 211/255, blue: 69/255, alpha: 1.0)
let orangeLesson = UIColor(red: 252/255, green: 130/255, blue: 77/255, alpha: 1.0)
let yellowLesson = UIColor(red: 253/255, green: 199/266, blue: 78/255, alpha: 1.0)

var arrUIColor = [greenLesson, yellowLesson, orangeLesson, grayBgView]   // Empty Array of type UIColor


//249	144	135
