//
//  CheckBox.swift
//  Sakai
//
//  Created by Thien Lan on 11/25/16.
//  Copyright © 2016 Team 4. All rights reserved.
//

import Foundation
import UIKit

class CheckBox:UIButton{
    // Images
    let checkedImage = UIImage(named: "isCheck")! as UIImage
    let uncheckedImage = UIImage(named: "unCheck")! as UIImage
    
    // Bool property
    var isChecked: Bool = false {
        didSet {
            if isChecked == true
            {
                self.setImage(checkedImage, for: .normal)
            }
            else
            {
                self.setImage(uncheckedImage, for: .normal)
            }
        }
    }
    
    override func awakeFromNib() {
        self.addTarget(self, action: #selector(buttonClicked(sender:)), for: UIControlEvents.touchUpInside)
        self.isChecked = false
    }
    
    func buttonClicked(sender: UIButton) {
        if sender == self
        {
            isChecked = !isChecked
        }
    }
}
