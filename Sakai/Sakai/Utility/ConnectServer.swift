//
//  ConnectServer.swift
//  Sakai
//
//  Created by Thien Lan on 11/25/16.
//  Copyright © 2016 Team 4. All rights reserved.
//

import Foundation
import Alamofire

class ConnectServer{
    
    private init(){}
    static var shareInstance = ConnectServer()
    
    func getData(url:String, completion:@escaping ((_ data:Data)->Void)){
        Alamofire.request(url, method: .get).responseJSON { (respone) in
            if respone.result.error == nil
            {
                if let json = respone.data {
                    completion(json)
                }
            }
        }
    }
    
    func GetQuizzApi(_ data: Data?) {
        do {
            var title:String?
            var time:String?
            var tempList:[Question] = []
            if let data = data, let response = try JSONSerialization.jsonObject(with: data, options:JSONSerialization.ReadingOptions(rawValue:0)) as? [String:AnyObject] {
                title = response["title"] as? String
                time = response["time"] as? String
                if let content = response["content"]
                {
                    if let listSection = content["listSection"]
                    {
                        for section in listSection as! [AnyObject]
                        {
                            if section is [String:AnyObject]
                            {
                                if let listQuestion = section["listQuestion"]
                                {
                                    for q in listQuestion as! [AnyObject]
                                    {
                                        if q is [String:AnyObject]
                                        {
                                            var temp = [String:String]()
                                            for a in q["listAnswer"] as! [AnyObject]
                                            {
                                                if a is [String:AnyObject]
                                                {
                                                    temp[a["key"] as! String] = a["value"] as? String
                                                }
                                                
                                            }
                                            tempList.append(Question.init(score: Int(q["score"] as! String)!,
                                                                          questionType: (q["questionType"] as! [String:AnyObject])["keyWord"] as! String,
                                                                          question: q["questionText"] as! String,
                                                                          answer: temp,
                                                                          correctAnswer: q["correctAnswer"] as! String))
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if time == nil
                {
                    time = "0"
                }
                GetQuizz.getQuizz = Quizz.init(title: title!, time: time!, listQuestion: tempList)
            }
        }catch let error as NSError {
            print("Error parsing results: \(error.localizedDescription)")
        }
    }
}
