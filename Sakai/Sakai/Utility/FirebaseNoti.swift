//
//  FirebaseNoti.swift
//  Sakai
//
//  Created by Thien Lan on 12/2/16.
//  Copyright © 2016 Team 4. All rights reserved.
//

import Foundation
import UserNotifications
import Firebase
import FirebaseMessaging

class FirebaseNoti: NSObject {
    
    private override init() {}
    static var shareInstance = FirebaseNoti()
    
    func registerNotification() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert,.badge,.sound]) { (granted:Bool, error:Error?) in
            if error != nil {
                print((error?.localizedDescription)!)
            }
            if granted {
                print("Permission granted")
            } else {
                print("Permission not granted")
            }
        }
        let action = UNNotificationAction(identifier: "action", title: "Download", options: [.foreground])
        let category = UNNotificationCategory(identifier: "actionCategory", actions: [action], intentIdentifiers: [], options: [])
        UNUserNotificationCenter.current().setNotificationCategories([category])
    }
}

extension FirebaseNoti: UNUserNotificationCenterDelegate
{
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
//        let userInfo = notification.request.content.userInfo
//        // Print message ID.
//        print("Message ID: \(userInfo["gcm.message_id"]!)")
//        // Print full message.
//        print(userInfo)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        print("Message ID: \(userInfo["gcm.message_id"]!)")
        // Print full message.
        print(userInfo)
    }
}

extension FirebaseNoti: FIRMessagingDelegate{
    func applicationReceivedRemoteMessage(_ remoteMessage: FIRMessagingRemoteMessage) {
        print(remoteMessage.appData)
    }
}
