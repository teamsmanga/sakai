//
//  DBFirebase.swift
//  Sakai
//
//  Created by Thien Lan on 12/2/16.
//  Copyright © 2016 Team 4. All rights reserved.
//

import Foundation
import FirebaseAuth
import FirebaseDatabase

class DBFirebase {
    private var ref: FIRDatabaseReference!
    
    private init() {
        ref = FIRDatabase.database().reference()
    }
    static var shareInstance = DBFirebase()
    
    func getData() {
        guard let userId = FIRAuth.auth()?.currentUser?.uid else { return }
        ref.child("\(userId)").observeSingleEvent(of: .value, with: {(snapshot) in
            guard let value = snapshot.value as? [String:AnyObject] else { return }
            GetUser.get = User.init(name: value["name"] as! String,
                                    age: value["age"] as! Int,
                                    email: value["email"] as! String,
                                    phone: value["phone"] as! String,
                                    gender: value["gender"] as! String)
        })
        { (error) in            
        }
    }
    
    // method get score
    func getGrade(idCourse: String, idLesson:String, completeHandle: @escaping (_ point:Int)->Void){
        guard let userId = FIRAuth.auth()?.currentUser?.uid else { return }
        ref.child("\(userId)/course/\(idCourse)/\(idLesson)").observeSingleEvent(of: .value, with: {(snapshot) in
            guard let value = snapshot.value as? [String:AnyObject] else { return }
            if let grade = value["point"] as? Int
            {
                completeHandle(grade)
            }
        })
    }
    
    func writeData(idCourse: String, idLesson:String, point: Int) {
        guard let userId = FIRAuth.auth()?.currentUser?.uid else { return }
        ref.child("\(userId)/course/\(idCourse)/\(idLesson)").setValue(["point":point])
    }
    
    func didQuiz(idCourse: String, idLesson:String, action:@escaping (_ isDone:Bool)->Void){
        guard let userId = FIRAuth.auth()?.currentUser?.uid else { return }
        ref.child("\(userId)/course/\(idCourse)/\(idLesson)").observeSingleEvent(of: .value, with: {(snapshot) in
            action(snapshot.hasChild("point"))
        })
    }
    
    enum DivisionError: Error {
        case ByZero
    }
}
