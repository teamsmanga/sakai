//
//  tableManager.swift
//  Sakai
//
//  Created by Developer on 11/25/16.
//  Copyright © 2016 Team 4. All rights reserved.
//

import UIKit

class tableManager: NSObject {
    
    // kiểm tra file sqlite có tồn tại hay chưa
    class func checkExistSqlite()->Bool {
        let fileManager = FileManager.default //
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let url = NSURL(fileURLWithPath: path)
        let filePath = url.appendingPathComponent("Sakai.sqlite")?.path
        //
        print("filePathSqlite: " + filePath!)
        //
        if fileManager.fileExists(atPath: filePath!) {
            _ = deleteDataSQLite() // Nếu sqlite có tồn tại thì delete tất cả record trong mỗi table
            return true // có tồn tại
        } else { // không tồn tại thì tạo các table mới
            self.createTableLesson()
            self.createTableVideo()
            self.createTableChapter()
            self.createTableChapterOffline()
            self.createTableUrlOfflineVideo()
            return false
        }
        
    }
    
    // thực hiện delete all record trong các table
    class func deleteDataSQLite()->Bool {
        var deleteStatement: OpaquePointer? = nil
        //
        var arrDeleteStatement = [String]()
        arrDeleteStatement.append("DELETE FROM Lesson;") // statement delete record in table lesson
        arrDeleteStatement.append("DELETE FROM Chapter;") // statement delete record in table chapter
        arrDeleteStatement.append("DELETE FROM Video;") // statement delete record in table video)
        //
        let db = openDatabase()
        //
        for i in 0...2 {
            if sqlite3_prepare_v2(db, arrDeleteStatement[i], -1, &deleteStatement, nil) == SQLITE_OK {
                if sqlite3_step(deleteStatement) == SQLITE_DONE {
                    print("Successfully deleted row.")
                    sqlite3_reset(deleteStatement)
                } else {
                    print("Query could not be prepared! \(String(cString:sqlite3_errmsg(db)))")
                }
            } else {
                print("Query could not be prepared! \(String(cString:sqlite3_errmsg(db)))")
            }
            
            sqlite3_finalize(deleteStatement)
        }
        
        return true
    }
    
    // thực hiện kết nối tới file sqlite
    class func openDatabase() -> OpaquePointer {
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let url = NSURL(fileURLWithPath: path)
        let filePath = url.appendingPathComponent("Sakai.sqlite")?.path
        
        var db: OpaquePointer? = nil
        if sqlite3_open(filePath, &db) == SQLITE_OK {
            print("Successfully opened connection to database at \(path)")
            return db!
        }
        return db!
    }
    
    // 1. tạo table lesson cho file sqlite mới
    class func createTableLesson() {
        let db = openDatabase()
        // 1. khởi tạo query tạo bảng
        let createTableString = "CREATE TABLE Lesson(" + " lesson_id Text NOT NULL," +
                                                         " lesson_user_id Text," +
                                                         " lesson_name Text," +
                                                         " lesson_logoPath Text," +
                                                         " lesson_dateStart DateTime," +
                                                         " lesson_teacher Text," +
                                                         " lesson_description Text);"
        // 2.
        var createTableStatement: OpaquePointer? = nil
        // 3.
        if sqlite3_prepare_v2(db, createTableString, -1, &createTableStatement, nil) == SQLITE_OK {
            if sqlite3_step(createTableStatement) == SQLITE_DONE {
                print("Contact table created.")
            } else {
                print("Contact table could not be created.")
            }
        } else {
            print("CREATE TABLE statement could not be prepared.")
        }
        // 4
        sqlite3_finalize(createTableStatement)
    }
    
    // 2. tạo table chapter cho file sqlite mới
    class func createTableChapter() {
        let db = openDatabase()
        // 1. khởi tạo query tạo bảng
        let createTableString = "CREATE TABLE Chapter(" +   " chapter_id Text NOT NULL," +
                                                            " chapter_lesson_id Text," +
                                                            " chapter_name Text," +
                                                            " chapter_logoPath Text," +
                                                            " chapter_dateStart DateTime," +
                                                            " chapter_teacher Text," +
                                                            " chapter_description Text);"
        // 2.
        var createTableStatement: OpaquePointer? = nil
        // 3.
        if sqlite3_prepare_v2(db, createTableString, -1, &createTableStatement, nil) == SQLITE_OK {
            if sqlite3_step(createTableStatement) == SQLITE_DONE {
                print("Contact table created.")
            } else {
                print("Contact table could not be created.")
            }
        } else {
            print("CREATE TABLE statement could not be prepared.")
        }
        // 4.
        sqlite3_finalize(createTableStatement)
    }
    
    // 3. tạo table video cho file sqlite mới
    class func createTableVideo() {
        let db = openDatabase()
        // 1. khởi tạo query tạo bảng
        let createTableString = "CREATE TABLE Video(" +     " video_id Text NOT NULL," +
                                                            " video_chapter_id Text," +
                                                            " video_name Text," +
                                                            " video_type Text," +
                                                            " video_url Text, " +
                                                            " video_url_offline Text);"
        // 2.
        var createTableStatement: OpaquePointer? = nil
        // 3.
        if sqlite3_prepare_v2(db, createTableString, -1, &createTableStatement, nil) == SQLITE_OK {
            if sqlite3_step(createTableStatement) == SQLITE_DONE {
                print("video table created.")
            } else {
                print("video table could not be created.")
            }
        } else {
            print("CREATE TABLE video statement could not be prepared.")
        }
        // 4.
        sqlite3_finalize(createTableStatement)
    }
    
    // 4.  tạo table lưu chapter offline
    class func createTableChapterOffline() {
        let db = openDatabase()
        // 1. khởi tạo query tạo bảng
        let createTableString = "CREATE TABLE ChapterOffline(" +    " chapter_id Text NOT NULL," +
                                                                    " lesson_chapter_id Text," +
                                                                    " chapter_name Text," +
                                                                    " chapter_logoPath Text," +
                                                                    " chapter_dateStart Text, " +
                                                                    " chapter_teacher Text);"
        // 2.
        var createTableStatement: OpaquePointer? = nil
        // 3.
        if sqlite3_prepare_v2(db, createTableString, -1, &createTableStatement, nil) == SQLITE_OK {
            if sqlite3_step(createTableStatement) == SQLITE_DONE {
                print("createTableChapterOffline table created.")
            } else {
                print("createTableChapterOffline table could not be created.")
            }
        } else {
            print("CREATE TABLE createTableChapterOffline statement could not be prepared.")
        }
        // 4.
        sqlite3_finalize(createTableStatement)
    }
    
    // 5. tạo table lưu url offline của video
    class func createTableUrlOfflineVideo() {
        let db = openDatabase()
        // 1. khởi tạo query tạo bảng
        let createTableString = "CREATE TABLE VideoOfflineUrl(" +    " video_id Text NOT NULL," +
                                                                        " video_chapter_id Text," +
                                                                        " video_name Text," +
                                                                        " video_type Text," +
                                                                        " video_url Text, " +
                                                                        " video_url_offline Text);"
        // 2.
        var createTableStatement: OpaquePointer? = nil
        // 3.
        if sqlite3_prepare_v2(db, createTableString, -1, &createTableStatement, nil) == SQLITE_OK {
            if sqlite3_step(createTableStatement) == SQLITE_DONE {
                print("createTableChapterOffline table created.")
            } else {
                print("createTableChapterOffline table could not be created.")
            }
        } else {
            print("CREATE TABLE createTableChapterOffline statement could not be prepared.")
        }
        // 4.
        sqlite3_finalize(createTableStatement)
    }
}
