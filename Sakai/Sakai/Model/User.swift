//
//  User.swift
//  Sakai
//
//  Created by Thien Lan on 12/2/16.
//  Copyright © 2016 Team 4. All rights reserved.
//

import Foundation
class User {
    var name:String?
    var age:Int?
    var email:String?
    var phone:String?
    var gender:String?
    
    init(name:String, age:Int, email:String, phone:String, gender:String) {
        self.name = name
        self.age = age
        self.email = email
        self.phone = phone
        self.gender = gender
    }
}

class GetUser {
    static var get:User?
}
