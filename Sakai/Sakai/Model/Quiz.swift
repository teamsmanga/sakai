//
//  Quiz.swift
//  Sakai
//
//  Created by Thien Lan on 11/24/16.
//  Copyright © 2016 Team 4. All rights reserved.
//

import Foundation

class Question {
    var score:Int?
    var questionType:String?
    var question:String?
    var answer:[String:String]?
    var correctAnswer:String?
    
    init(){}
    
    init(score:Int, questionType:String, question:String, answer:[String:String], correctAnswer:String) {
        self.score = score
        self.questionType = questionType
        self.question = question
        self.answer = answer
        self.correctAnswer = correctAnswer
    }
}

class Quizz{
    var title:String?
    var time:String?
    var listQuestion:[Question]?
    
    init(){}
    
    init(title:String, time:String, listQuestion:[Question]) {
        self.title = title
        self.time = time
        self.listQuestion = listQuestion
    }
}

class GetQuizz{
    static var getQuizz = Quizz()
}
