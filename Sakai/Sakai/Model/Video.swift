//
//  Video.swift
//  Sakai_demo
//
//  Created by Developer on 11/24/16.
//  Copyright © 2016 Team 4. All rights reserved.
//

import UIKit

class Video: NSObject {
    var video_id:String = ""
    var video_chapter_id:String = ""
    var video_name:String = ""
    var video_type:String = ""
    var video_url:String = ""
    var video_url_offline:String = ""
    
    override init() {
        self.video_id = ""
        self.video_chapter_id = ""
        self.video_name = ""
        self.video_type = ""
        self.video_url = ""
        self.video_url_offline = ""
    
    }
    
    // MARK: 1. save all video to SQLite
    // input: [video]
    // output:
    class func saveVideosToSQLite(_ arrVideo:[Video]) {
        // 1.
        let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent("Sakai.sqlite")
        // 2.
        let insertStatementString = "INSERT INTO Video (     video_id, "            +
                                                            " video_chapter_id, "    +
                                                            " video_name,"           +
                                                            " video_type, "          +
                                                            " video_url) "            +
                                                            " VALUES (?, ?, ?, ?, ?);"
        // 4.
        var insertStatement: OpaquePointer? = nil
        // 5.
        var db: OpaquePointer? = nil
        if sqlite3_open(fileURL.path, &db) == SQLITE_OK {
            if sqlite3_prepare_v2(db, insertStatementString, -1, &insertStatement, nil) == SQLITE_OK {
                for item in arrVideo {
                    //
                    let id:NSString = NSString(string:item.video_id)
                    let chapter_id:NSString = NSString(string:item.video_chapter_id)
                    let name:NSString = NSString(string:item.video_name)
                    let type:NSString = NSString(string:item.video_type)
                    let url:NSString = NSString(string:item.video_url)
                    //
                    sqlite3_bind_text(insertStatement, 1, id.utf8String, -1, nil)
                    sqlite3_bind_text(insertStatement, 2, chapter_id.utf8String, -1, nil)
                    sqlite3_bind_text(insertStatement, 3, name.utf8String, -1, nil)
                    sqlite3_bind_text(insertStatement, 4, type.utf8String, -1, nil)
                    sqlite3_bind_text(insertStatement, 5, url.utf8String, -1, nil)
                    //
                    if sqlite3_step(insertStatement) == SQLITE_DONE {
                        print("Successfully inserted row.")
                        sqlite3_reset(insertStatement)
                    } else {
                        print("Could not insert row.")
                    }
                }
            } else {
                print("INSERT statement could not be prepared.")
            }
            sqlite3_finalize(insertStatement)
            sqlite3_close(db)
        }
    }
    
    // MARK: 2. get video by id chapter
    // input: idChapter
    // output: [video]
    class func getVideoByIdChapter(_ idChapter:String)->[Video] {
        var arrVideos = [Video]()
        let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent("Sakai.sqlite")
        let queryStatementString = String(format: "SELECT * FROM Video WHERE video_chapter_id = '%@'", idChapter)
        var queryStatement: OpaquePointer? = nil
        var db: OpaquePointer? = nil
        // 1
        if sqlite3_open(fileURL.path, &db) == SQLITE_OK {
            if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
                // 2
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    let video = Video()
                    //
                    let ColId = sqlite3_column_text(queryStatement, 0); video.video_id = String(cString:ColId!)
                    let ColChapterId = sqlite3_column_text(queryStatement, 1); video.video_chapter_id = String(cString:ColChapterId!)
                    let ColName = sqlite3_column_text(queryStatement, 2); video.video_name = String(cString:ColName!)
                    let ColType = sqlite3_column_text(queryStatement, 3); video.video_type = String(cString:ColType!)
                    let ColUrlPath = sqlite3_column_text(queryStatement, 4); video.video_url = String(cString:ColUrlPath!)
                    //
                    arrVideos.append(video)
                }
                return arrVideos
            } else {
                print("SELECT statement could not be prepared")
            }
            // 6
            sqlite3_finalize(queryStatement)
        }
        return arrVideos
    }
    
    // MARK: 3. save offline video's url
    // input: id video, urlOffline video
    // output : true/false
    class func saveUrlOfflineVideo(_ idVideo:String, _ urlOnlineVideo:String, _ urlOfflineVideo:String)->Bool {
        let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent("Sakai.sqlite")
        
        let updateStatementString = String(format: "INSERT INTO VideoOfflineUrl (video_id, video_url, video_url_offline)" +
            "VALUES ('%@', '%@', '%@');", idVideo, urlOnlineVideo, urlOfflineVideo)

        var updateStatement: OpaquePointer? = nil
        var db:OpaquePointer? = nil
        if sqlite3_open(fileURL.path, &db) == SQLITE_OK {
            if sqlite3_prepare_v2(db, updateStatementString, -1, &updateStatement, nil) == SQLITE_OK {
                if sqlite3_step(updateStatement) == SQLITE_DONE {
                    print("Video offline saved row.")
                } else {
                    print("Could not update row.")
                }
            } else {
                print("UPDATE statement could not be prepared")
            }
            sqlite3_finalize(updateStatement)
            sqlite3_close(db)
        }
        return true
    }
    
    // MARK: 4. remove url cho video by idvideo
    // intput: idVideo
    // output: true/false
    class func removeUrlOfflineVideo(_ idVideo:String)->Bool {
        let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent("Sakai.sqlite")
        let updateStatementString = String(format: "DELETE FROM VideoOfflineUrl WHERE video_id = '%@'", idVideo)
        var updateStatement: OpaquePointer? = nil
        var db:OpaquePointer? = nil
        if sqlite3_open(fileURL.path, &db) == SQLITE_OK {
            if sqlite3_prepare_v2(db, updateStatementString, -1, &updateStatement, nil) == SQLITE_OK {
                if sqlite3_step(updateStatement) == SQLITE_DONE {
                    print("Successfully updated row.")
                } else {
                    print("Query could not be prepared! \(String(cString:sqlite3_errmsg(db)))")
                }
            } else {
                print("Query could not be prepared! \(String(cString:sqlite3_errmsg(db)))")
            }
            sqlite3_finalize(updateStatement)
            sqlite3_close(db)
        }
        return true
    }
    
    // MARK: 5. get link url offline of video
    // input: idVideoChapter:String, idVideo: String
    // output: urlOfflineVideo:String
    class func checkVideoOfflineIsExist(_ idVideo:String)->Int {
        var numberVideoExist = Int()
        let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent(nameFileSqlite)
        let queryStatementString = String(format: "SELECT COUNT(video_url_offline) from VideoOfflineUrl where  video_id = '%@'", idVideo)
        var queryStatement: OpaquePointer? = nil
        var db: OpaquePointer? = nil
        // 1
        if sqlite3_open(fileURL.path, &db) == SQLITE_OK {
            if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
                // 2
                if (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    numberVideoExist = Int(sqlite3_column_int(queryStatement, 0))
                }
                else {
                    print("Query could not be prepared! \(String(cString:sqlite3_errmsg(db)))")
                }
            }
            else {
                print("Query could not be prepared! \(String(cString:sqlite3_errmsg(db)))")
            }
        }
        sqlite3_reset(queryStatement)
        sqlite3_finalize(queryStatement)
        sqlite3_close(db)
        return numberVideoExist
    }
    
    // MARK: 5. get link url offline of video
    // input: idVideoChapter:String, idVideo: String
    // output: urlOfflineVideo:String
    class func getVideoOfflineByIdVideo(_ idVideo:String)->Video {
        let video = Video()
        let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent("Sakai.sqlite")
        let queryStatementString = String(format: "SELECT * FROM VideoOfflineUrl WHERE video_id = '%@'", idVideo)
        var queryStatement: OpaquePointer? = nil
        var db: OpaquePointer? = nil
        // 1
        if sqlite3_open(fileURL.path, &db) == SQLITE_OK {
            if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
                // 2
                if (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    //
                    let ColId = sqlite3_column_text(queryStatement, 0); video.video_id = String(cString:ColId!)
                    let ColUrlPath = sqlite3_column_text(queryStatement, 4); video.video_url = String(cString:ColUrlPath!)
                    let ColOfflineUrlPath = sqlite3_column_text(queryStatement, 5); video.video_url_offline = String(cString:ColOfflineUrlPath!)
                    //
                }
            } else {
                print("SELECT statement could not be prepared")
            }
            // 6
            sqlite3_finalize(queryStatement)
        }
        return video
        
        
//        
//        //----------------------------------------------------
//        let video = Video()
//        let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent("Sakai.sqlite")
//        let queryStatementString = String(format: "SELECT * from VideoOfflineUrl where video_id = '%@'", idVideo)
//        var queryStatement: OpaquePointer? = nil
//        var db: OpaquePointer? = nil
//        // 1
//        if sqlite3_open(fileURL.path, &db) == SQLITE_OK {
//            if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
//                // 2
//                if (sqlite3_step(queryStatement) == SQLITE_ROW) {
//                    let ColId = sqlite3_column_text(queryStatement, 0); video.video_id = String(cString:ColId!)
//                    let ColVideoChapterId = sqlite3_column_text(queryStatement, 1); video.video_chapter_id = String(cString:ColVideoChapterId!)
//                    let ColUrl = sqlite3_column_text(queryStatement, 4); video.video_url = String(cString:ColUrl!)
//                    let ColUrlOffline = sqlite3_column_text(queryStatement, 5); video.video_url = String(cString:ColUrlOffline!)
//                }
//            } else {
//                print("SELECT statement could not be prepared")
//            }
//            // 6
//            sqlite3_finalize(queryStatement)
//        }
//        return video
    }
}
