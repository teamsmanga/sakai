//
//  Lesson.swift
//  Sakai
//
//  Created by Developer on 11/23/16.
//  Copyright © 2016 Team 4. All rights reserved.
//

import UIKit

class Lesson: NSObject {
    var lesson_id:String?
    var lesson_user_id:String?
    var lesson_name:String?
    var lesson_logoPath:String?
    var lesson_dateStart:Date?
    var lesson_teacher:String?
    var lesson_description:String?
    lazy var lesson_chapter = [Chapter]()
    
    override init() {
        self.lesson_id = ""
        self.lesson_user_id = ""
        self.lesson_name = ""
        self.lesson_logoPath = ""
        self.lesson_dateStart = nil
        self.lesson_description = ""
    }
    
    // MARK: 1. save all Lessons from JSON to SQLite
    // input: array Lessons
    // output:
    class func saveLessonsToSQLite(_ arrLessons:[Lesson]) {
        // 1.
        let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent("Sakai.sqlite")
        // 2.
        var insertStatement: OpaquePointer? = nil
        // 3.
        var db: OpaquePointer? = nil
        // 4.
        for item in arrLessons {
            let insertStatementString = String(format: "INSERT INTO Lesson (" + "lesson_id, "           +
                                                                                "lesson_user_id, "      +
                                                                                "lesson_name,"          +
                                                                                "lesson_logoPath, "     +
                                                                                "lesson_teacher, "      +
                                                                                "lesson_description) "  +
                        "VALUES ('%@', '%@', '%@', '%@', '%@', '%@');", item.lesson_id!,
                                                                              item.lesson_user_id!,
                                                                              item.lesson_name!,
                                                                              item.lesson_logoPath!,
                                                                              item.lesson_teacher!,
                                                                              item.lesson_description!)
            //
            if sqlite3_open(fileURL.path, &db) == SQLITE_OK {
                if sqlite3_prepare_v2(db, insertStatementString, -1, &insertStatement, nil) == SQLITE_OK {
                    if sqlite3_step(insertStatement) == SQLITE_DONE {
                        print("Lesson Successfully inserted row.")
                        sqlite3_reset(insertStatement)
                    } else {
                        print(String(cString: sqlite3_errmsg(db)))
                    }
                }
            } else {
                print(String(cString: sqlite3_errmsg(db)))
            }
            sqlite3_finalize(insertStatement)
            sqlite3_close(db)
        }
        sqlite3_close(db)
    }
    
    // MARK: 2. get all records from table Lesson
    // input:
    // output: array lessons
    class func getLessonsFromSQLite()->[Lesson] {
        var arrLesson = [Lesson]()
        let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent("Sakai.sqlite")
        let queryStatementString = "SELECT * FROM Lesson;"
        var queryStatement: OpaquePointer? = nil
        var db: OpaquePointer? = nil
        // 1
        if sqlite3_open(fileURL.path, &db) == SQLITE_OK {
            if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
                // 2
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    let lesson = Lesson()
                    let queryResultColId = sqlite3_column_text(queryStatement, 0); lesson.lesson_id = String(cString:queryResultColId!)
                    let queryResultColName = sqlite3_column_text(queryStatement, 2); lesson.lesson_name = String(cString:queryResultColName!)
                    let queryResultColLogoPath = sqlite3_column_text(queryStatement, 3); lesson.lesson_logoPath = String(cString:queryResultColLogoPath!)
                    let queryResultColTeacher = sqlite3_column_text(queryStatement, 5); lesson.lesson_teacher = String(cString:queryResultColTeacher!)
                    let queryResultColDescription = sqlite3_column_text(queryStatement, 6); lesson.lesson_description = String(cString:queryResultColDescription!)
                    arrLesson.append(lesson)
                }
//                return arrLesson
            } else {
                print("Query could not be prepared! \(String(cString:sqlite3_errmsg(db)))")
            }
            // 6
            
        }
        sqlite3_finalize(queryStatement)
        sqlite3_close(db)
        return arrLesson
    }
    
    // MARK: 3. get lesson by id lesson
    // input: id lesson
    // output: lesson
    class func getLessonByIdLesson(_ idLesson:String)->Lesson {
        let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent("Sakai.sqlite")
        let queryStatementString = String(format: "SELECT * FROM Lesson WHERE lesson_id = '%@';", idLesson)
        var queryStatement: OpaquePointer? = nil
        var db: OpaquePointer? = nil
        let currentLesson = Lesson()
        // 1
        if sqlite3_open(fileURL.path, &db) == SQLITE_OK {
            if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
                // 2
                if sqlite3_step(queryStatement) == SQLITE_ROW {
                    //
                    let ColId = sqlite3_column_text(queryStatement, 0); currentLesson.lesson_id = String(cString:ColId!)
                    let ColUserId = sqlite3_column_text(queryStatement, 1); currentLesson.lesson_user_id = String(cString:ColUserId!)
                    let ColName = sqlite3_column_text(queryStatement, 2); currentLesson.lesson_name = String(cString:ColName!)
                    let ColLogoPath = sqlite3_column_text(queryStatement, 3); currentLesson.lesson_logoPath = String(cString:ColLogoPath!)
                    let ColTeacher = sqlite3_column_text(queryStatement, 5); currentLesson.lesson_teacher = String(cString:ColTeacher!)
                    let ColDescription = sqlite3_column_text(queryStatement, 6); currentLesson.lesson_description = String(cString:ColDescription!)
                    //
//                    sqlite3_finalize(queryStatement)
//                    sqlite3_close(db)
//                    return currentLesson
                    //
                } else {
                    print("Query returned no results")
                }
            } else {
                let errorMessage = String(cString:sqlite3_errmsg(db))
                print("Query could not be prepared! \(errorMessage)")
            }
            // 6
            sqlite3_finalize(queryStatement)
            sqlite3_close(db)
        }
        return currentLesson
    }
    
    // MARK: 4. get array of idLesson saved in table ChapterOffline
    // input: 
    // output: array id lesson string
    class func getIdLessonOffline()->[String] {
        var arrIdLesson = [String]()
        let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent(nameFileSqlite)
        let queryStatementString = "SELECT lesson_chapter_id FROM ChapterOffline GROUP BY lesson_chapter_id ORDER BY lesson_chapter_id ASC"
        var queryStatement: OpaquePointer? = nil
        var db: OpaquePointer? = nil
        // 1
        if sqlite3_open(fileURL.path, &db) == SQLITE_OK {
            if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
                // 2
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    let ColIdLesson = sqlite3_column_text(queryStatement, 0); let strId = String(cString:ColIdLesson!)
                    //
                    arrIdLesson.append(strId)
                }
//                sqlite3_finalize(queryStatement)
//                sqlite3_close(db)
//                return arrIdLesson
            } else {
                print("SELECT statement could not be prepared")
            }
            // 6
        }
        sqlite3_finalize(queryStatement)
        sqlite3_close(db)
        return arrIdLesson
    }
}
