//
//  Chapter.swift
//  Sakai_demo
//
//  Created by Developer on 11/24/16.
//  Copyright © 2016 Team 4. All rights reserved.
//

import UIKit

class Chapter: NSObject {
    var chapter_id:String?          // id chapter
    var chapter_lesson_id:String?   // idLesson is the foreign key between table "chapter" and table "lesson"
    var chapter_name:String?        // chapter name
    var chapter_logoPath:String?    // the chapter logo urlPath
    var chapter_dateStart:Date? // the start time
    var chapter_teacher:String?     // the chapter's teacher name
    var chapter_description:String? // the chapter's description
    lazy var chapter_video = [Video]()
    
    override init() {
        self.chapter_id = ""
        self.chapter_lesson_id = ""
        self.chapter_name = ""
        self.chapter_logoPath = ""
        self.chapter_dateStart = nil
        self.chapter_teacher = ""
        self.chapter_description = ""
    }

    // MARK: 1. save all chapters to Sqlite
    // input: array chapters
    // output:
    class func saveChaptersToSQLite(_ arrChapter:[Chapter]) {
        // 1.
        let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent(nameFileSqlite)
        // 2.
        var insertStatement: OpaquePointer? = nil
        // 3.
        var db: OpaquePointer? = nil
        // 4.
        for item in arrChapter {
            let insertStatementString = String(format: "INSERT INTO Chapter (" + "chapter_id, " +
                                                                                    "chapter_lesson_id, "      +
                                                                                    "chapter_name,"          +
                                                                                    "chapter_logoPath, "     +
                                                                                    "chapter_dateStart, "    +
                                                                                    "chapter_teacher, "      +
                                                                                    "chapter_description) "  +
                "VALUES ('%@', '%@', '%@', '%@', '%@', '%@', '%@');", item.chapter_id!,
                                                                      item.chapter_lesson_id!,
                                                                      item.chapter_name!,
                                                                      item.chapter_logoPath!,
                                                                      "",
//                                                                      item.chapter_dateStart! as CVarArg,
                                                                      item.chapter_teacher!,
                                                                      item.chapter_description!)
            //
            if sqlite3_open(fileURL.path, &db) == SQLITE_OK {
                if sqlite3_prepare_v2(db, insertStatementString, -1, &insertStatement, nil) == SQLITE_OK {
                    if sqlite3_step(insertStatement) == SQLITE_DONE {
                        print("chapter Successfully inserted row.")
                        sqlite3_close(db)
                        sqlite3_reset(insertStatement)
                    } else {
                        print("save Chapter" + String(cString: sqlite3_errmsg(db)))
                        sqlite3_close(db)
                    }
                }
                sqlite3_reset(insertStatement)
            } else {
                print(String(cString: sqlite3_errmsg(db)))
                sqlite3_close(db)
            }
            sqlite3_finalize(insertStatement)
            sqlite3_close(db)
        }
    }
    
    // MARK: 2. get all chapters in sqlite
    // input:
    // output: [Chapter]
    class func getAllChapters()->[Chapter] {
        var arrChapters = [Chapter]()
        let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent(nameFileSqlite)
        let queryStatementString = "SELECT * FROM Chapter"
        var queryStatement: OpaquePointer? = nil
        var db: OpaquePointer? = nil
        // 1
        if sqlite3_open(fileURL.path, &db) == SQLITE_OK {
            if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
                // 2
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    let chapter = Chapter()
                    //
                    let ColId = sqlite3_column_text(queryStatement, 0); chapter.chapter_id = String(cString:ColId!)
                    let ColLessonId = sqlite3_column_text(queryStatement, 1); chapter.chapter_lesson_id = String(cString:ColLessonId!)
                    let ColName = sqlite3_column_text(queryStatement, 2); chapter.chapter_name = String(cString:ColName!)
                    let ColLogoPath = sqlite3_column_text(queryStatement, 3); chapter.chapter_logoPath = String(cString:ColLogoPath!)
                    let ColTeacher = sqlite3_column_text(queryStatement, 5); chapter.chapter_teacher = String(cString:ColTeacher!)
                    let ColDescription = sqlite3_column_text(queryStatement, 6); chapter.chapter_description = String(cString:ColDescription!)
                    //
                    arrChapters.append(chapter)
                }
//                sqlite3_reset(queryStatement)
//                sqlite3_finalize(queryStatement)
//                sqlite3_close(db)
//                return arrChapters
            } else {
                print("SELECT statement could not be prepared")
            }
        }
        // 6
        sqlite3_reset(queryStatement)
        sqlite3_finalize(queryStatement)
        sqlite3_close(db)
        return arrChapters
    }
    
    // MARK: 3. get array chapters by id lesson
    // input: id lesson
    // output: [Chapter]
    class func getChaptersByIdLesson(_ idLesson:String)->[Chapter] {
        var arrChapters = [Chapter]()
        let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent(nameFileSqlite)
        let queryStatementString = String(format: "SELECT * FROM Chapter WHERE chapter_lesson_id = '%@'", idLesson)
        var queryStatement: OpaquePointer? = nil
        var db: OpaquePointer? = nil
        // 1
        if sqlite3_open(fileURL.path, &db) == SQLITE_OK {
            if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
                // 2
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    let chapter = Chapter()
                    //
                    let ColId = sqlite3_column_text(queryStatement, 0); chapter.chapter_id = String(cString:ColId!)
                    let ColLessonId = sqlite3_column_text(queryStatement, 1); chapter.chapter_lesson_id = String(cString:ColLessonId!)
                    let ColName = sqlite3_column_text(queryStatement, 2); chapter.chapter_name = String(cString:ColName!)
                    let ColLogoPath = sqlite3_column_text(queryStatement, 3); chapter.chapter_logoPath = String(cString:ColLogoPath!)
                    //
//                    let ColDateStart = sqlite3_column_double(queryStatement, Int32(4))
                    
//                    chapter.chapter_dateStart = ColDateStart
                    //
                    let ColTeacher = sqlite3_column_text(queryStatement, 5); chapter.chapter_teacher = String(cString:ColTeacher!)
                    let ColDescription = sqlite3_column_text(queryStatement, 6); chapter.chapter_description = String(cString:ColDescription!)
                    
                    arrChapters.append(chapter)
                }
//                sqlite3_reset(queryStatement)
//                sqlite3_finalize(queryStatement)
//                sqlite3_close(db)
//                return arrChapters
            } else {
                print("SELECT statement could not be prepared")
            }
            // 6
            sqlite3_reset(queryStatement)
            sqlite3_finalize(queryStatement)
            sqlite3_close(db)
        }
        return arrChapters
    }
    
    // MARK: 4. get chapter by id chapter
    // input: id chapter
    // output: chapter
    class func getChaptersByIdChapter(_ idChapter:String)->Chapter {
        let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent(nameFileSqlite)
        let queryStatementString = String(format: "SELECT * FROM Chapter WHERE chapter_id = '%@'", idChapter)
        var queryStatement: OpaquePointer? = nil
        var db: OpaquePointer? = nil
        let chapter = Chapter()
        // 1
        if sqlite3_open(fileURL.path, &db) == SQLITE_OK {
            if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
                // 2
                if sqlite3_step(queryStatement) == SQLITE_ROW {
                    //
                    let ColId = sqlite3_column_text(queryStatement, 0); chapter.chapter_id = String(cString:ColId!)
                    let ColLessonId = sqlite3_column_text(queryStatement, 1); chapter.chapter_lesson_id = String(cString:ColLessonId!)
                    let ColName = sqlite3_column_text(queryStatement, 2); chapter.chapter_name = String(cString:ColName!)
                    let ColLogoPath = sqlite3_column_text(queryStatement, 3); chapter.chapter_logoPath = String(cString:ColLogoPath!)
                    let ColTeacher = sqlite3_column_text(queryStatement, 5); chapter.chapter_teacher = String(cString:ColTeacher!)
                    let ColDescription = sqlite3_column_text(queryStatement, 6); chapter.chapter_description = String(cString:ColDescription!)
                }
            } else {
                print("SELECT statement could not be prepared")
            }
        }
        sqlite3_reset(queryStatement)
        sqlite3_finalize(queryStatement)
        sqlite3_close(db)
        return chapter
    }
    
    // MARK: 5. save chapter offline by id chapter
    // input: id lesson
    // output: true/false
    class func saveOfflineChapter(_ chapter:Chapter)->Bool {
        // 0.
        let number = checkChapterOfflineIsExist(chapter.chapter_id!, chapter.chapter_lesson_id!)
        if number > 0 {
            return false
        }
        
        // 1.
        var retr = true
        let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent(nameFileSqlite)
        // 2.
        let id:NSString = NSString(string:chapter.chapter_id!)
        let lesson_id:NSString = NSString(string:chapter.chapter_lesson_id!)
        let name:NSString = NSString(string:chapter.chapter_name!)
        let logoPath:NSString = NSString(string:chapter.chapter_logoPath!)
//        let dateStart = chapter.chapter_dateStart
        let dateStart = "22/11/2016"
        let teacher:NSString = NSString(string:chapter.chapter_teacher!)
        let descript:NSString = NSString(string:chapter.chapter_description!)
        //
        let insertStatementString =  String(format: "INSERT INTO ChapterOffline (   chapter_id, " +
            "lesson_chapter_id, " +
            "chapter_name," +
            "chapter_logoPath, " +
            "chapter_dateStart, " +
            "chapter_teacher) " +
            "VALUES ('%@', '%@', '%@', '%@', '%@', '%@');", id, lesson_id, name, logoPath, dateStart as CVarArg, teacher, descript)
        // 4.
        var insertStatement: OpaquePointer? = nil
        // 5.
        var db: OpaquePointer? = nil
        if sqlite3_open(fileURL.path, &db) == SQLITE_OK {
            if sqlite3_prepare_v2(db, insertStatementString, -1, &insertStatement, nil) == SQLITE_OK {
                //
                if sqlite3_step(insertStatement) == SQLITE_DONE {
                    print("Successfully inserted row.")
                    retr = true
                } else {
                    print("saveOfflineChapter: " + String(cString: sqlite3_errmsg(db)))
                    retr = false
                }
            } else {
                print(String(cString: sqlite3_errmsg(db)))
                retr = false
            }
            sqlite3_reset(insertStatement)
        }
        sqlite3_finalize(insertStatement)
        sqlite3_close(db)
        return retr
    }
    
    // MARK: 6. remove chapter offline
    // input: id lesson
    // output:
    class func removeOfflineChapter(_ chapter:Chapter) {
        // 1.
        let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent(nameFileSqlite)
        // 2.
        let deleteStatementString =  String(format: "delete from ChapterOffline where chapter_id = '%@' and lesson_chapter_id = '%@'", chapter.chapter_id!, chapter.chapter_lesson_id!)
        // 3.
        var deleteStatement: OpaquePointer? = nil
        // 4.
        var db: OpaquePointer? = nil
        if sqlite3_open(fileURL.path, &db) == SQLITE_OK {
            if sqlite3_prepare_v2(db, deleteStatementString, -1, &deleteStatement, nil) == SQLITE_OK {
                //
                if sqlite3_step(deleteStatement) == SQLITE_DONE {
                    print("Successfully delete offline chapter row.")
                    sqlite3_reset(deleteStatement)
                } else {
                    print(String(cString: sqlite3_errmsg(db)))
                }
            } else {
                print(String(cString: sqlite3_errmsg(db)))
                
            }
        }
        sqlite3_reset(deleteStatement)
        sqlite3_finalize(deleteStatement)
        sqlite3_close(db)
    }
    
    // MARK: 7. check whether chapter saved or not
    // input: primary key idChapter, foreign key id_lesson_chapter
    // output: 1: true, 0: false
    class func checkChapterOfflineIsExist(_ idChapter:String, _ idLessChapter:String)->Int {
        var numberChapter = Int()
        let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent(nameFileSqlite)
        let queryStatementString = String(format: "SELECT count(chapter_id) from chapterOffline where chapter_id = '%@' and lesson_chapter_id = '%@'", idChapter, idLessChapter)
        var queryStatement: OpaquePointer? = nil
        var db: OpaquePointer? = nil
        // 1
        if sqlite3_open(fileURL.path, &db) == SQLITE_OK {
            if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
                // 2
                if (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    numberChapter = Int(sqlite3_column_int(queryStatement, 0))
                }
                else {
                    print("Query could not be prepared! \(String(cString:sqlite3_errmsg(db)))")
                }
            }
            else {
                print("Query could not be prepared! \(String(cString:sqlite3_errmsg(db)))")
            }
        }
        sqlite3_reset(queryStatement)
        sqlite3_finalize(queryStatement)
        sqlite3_close(db)
        return numberChapter
    }
    
    // MARK : 8. get all chapter offline by id lesson
    // input: idlesson
    // output: [Chapter]
    class func getAllChaptersOfflineByIdLesson(_ idLesson:String)->[Chapter] {
        var arrChapters = [Chapter]()
        let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent("Sakai.sqlite")
        let queryStatementString =  String(format:  "SELECT * FROM ChapterOffline " +
                                                    "WHERE lesson_chapter_id = '%@' " +
                                                    "ORDER BY lesson_chapter_id ASC ", idLesson)
        var queryStatement: OpaquePointer? = nil
        var db: OpaquePointer? = nil
        // 1
        if sqlite3_open(fileURL.path, &db) == SQLITE_OK {
            if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
                // 2
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    let chapter = Chapter()
                    //
                    let ColId = sqlite3_column_text(queryStatement, 0); chapter.chapter_id = String(cString:ColId!)
                    let ColLessonId = sqlite3_column_text(queryStatement, 1); chapter.chapter_lesson_id = String(cString:ColLessonId!)
                    let ColName = sqlite3_column_text(queryStatement, 2); chapter.chapter_name = String(cString:ColName!)
                    let ColLogoPath = sqlite3_column_text(queryStatement, 3); chapter.chapter_logoPath = String(cString:ColLogoPath!)
                    //
//                    let ColDate = sqlite3_column_text(queryStatement, 4)
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd"
                    let date = dateFormatter.date(from: "20/11/2016")
                    chapter.chapter_dateStart = date
                    //
                    let ColTeacher = sqlite3_column_text(queryStatement, 5); chapter.chapter_teacher = String(cString:ColTeacher!)
                    //
                    arrChapters.append(chapter)
                }
            } else {
                print("Query could not be prepared! \(String(cString:sqlite3_errmsg(db)))")
            }
            // 6
        }
        sqlite3_reset(queryStatement)
        sqlite3_finalize(queryStatement)
        sqlite3_close(db)
        return arrChapters
    }
    
    // MARK: 9. get number lesson from table "chapterOffline" with getting rid of duplicated record
    // intput: 
    // output: the number lesson
    class func getNumberLesson()->Int {
        var numberLesson = Int()
        let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent(nameFileSqlite)
        let queryStatementString = String(format: "SELECT Count(DISTINCT lesson_chapter_id) FROM chapterOffline")
        var queryStatement: OpaquePointer? = nil
        var db: OpaquePointer? = nil
        // 1
        if sqlite3_open(fileURL.path, &db) == SQLITE_OK {
            if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
                // 2
                if (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    numberLesson = Int(sqlite3_column_int(queryStatement, 0))
                }
                else {
                    print("Query could not be prepared! \(String(cString:sqlite3_errmsg(db)))")
                }
            }
            else {
                print("Query could not be prepared! \(String(cString:sqlite3_errmsg(db)))")
            }
            sqlite3_reset(queryStatement)
        }
        sqlite3_finalize(queryStatement)
        sqlite3_close(db)
        return numberLesson
    }
    
    // MARK: 10. get all chapter in table chapterOffline
    // input: 
    // output: [Chapter]
    class func getAllChaptersOffline()->[Chapter] {
        var arrChapters = [Chapter]()
        let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent(nameFileSqlite)
        let queryStatementString =  "SELECT * FROM ChapterOffline"
        var queryStatement: OpaquePointer? = nil
        var db: OpaquePointer? = nil
        // 1
        if sqlite3_open(fileURL.path, &db) == SQLITE_OK {
            if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
                // 2
                while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                    let chapter = Chapter()
                    //
                    let ColId = sqlite3_column_text(queryStatement, 0); chapter.chapter_id = String(cString:ColId!)
                    let ColLessonId = sqlite3_column_text(queryStatement, 1); chapter.chapter_lesson_id = String(cString:ColLessonId!)
                    let ColName = sqlite3_column_text(queryStatement, 2); chapter.chapter_name = String(cString:ColName!)
                    let ColLogoPath = sqlite3_column_text(queryStatement, 3); chapter.chapter_logoPath = String(cString:ColLogoPath!)
                    //
                    //                    let ColDate = sqlite3_column_text(queryStatement, 4)
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd"
                    let date = dateFormatter.date(from: "20/11/2016")
                    chapter.chapter_dateStart = date
                    //
                    let ColTeacher = sqlite3_column_text(queryStatement, 5); chapter.chapter_teacher = String(cString:ColTeacher!)
                    //
                    arrChapters.append(chapter)
                }
            } else {
                print("Query could not be prepared! \(String(cString:sqlite3_errmsg(db)))")
            }
            // 6
        }
        sqlite3_reset(queryStatement)
        sqlite3_finalize(queryStatement)
        sqlite3_close(db)
        return arrChapters
    }
}
