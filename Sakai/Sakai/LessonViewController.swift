//
//  LessonViewController.swift
//  Sakai
//
//  Created by dang hung on 11/24/16.
//  Copyright © 2016 Team 4. All rights reserved.
//

import UIKit
import BMPlayer

class LessonViewController: UIViewController, DownloadViewControllerDelegate  {
    @IBOutlet weak var player: UIView!
    var arrVideo = [Video]()
    var videoOffline = Video()
    var idVideo:String?
    var data : Chapter?
    
    @IBOutlet weak var btnDownLoad: UIButton!
    
    @IBOutlet weak var btnQuiz: UIButton!
    
    @IBOutlet weak var tvDescription: UITextView!
    
    @IBOutlet weak var lbTeacher: UILabel!
    
    @IBOutlet weak var lbTime: UILabel!
    
    @IBOutlet weak var nameCourses: UILabel!
    
    @IBOutlet weak var viewTitle: UIView!
    
    var idChapter:String! = String()
    
    var idCourse:String! = String()
    
    var progressDownload: Float?
    var progressView = UIProgressView()
    
    var customPlayer = PlayerCustomView()
    var customWebView = WebView()
    
    var numberVideoExist:Int? = nil
    var videoType = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        btnQuiz.layer.cornerRadius = 5.0
        btnQuiz.backgroundColor = orangeSwift
        self.nameCourses.textColor = orangeSwift
        viewTitle.layer.cornerRadius = 5.0
        
        btnDownLoad.layer.cornerRadius = 5.0
        btnDownLoad.backgroundColor = orangeSwift
        
        
        arrVideo = Video.getVideoByIdChapter(idChapter)
        
        idCourse = Chapter.getChaptersByIdChapter(idChapter).chapter_lesson_id //get id course by IDChapter
        
        data = Chapter.getChaptersByIdChapter(idChapter)
        DBFirebase.shareInstance.didQuiz(idCourse: idCourse, idLesson: idChapter, action: {[weak self](isDone) in
            self?.btnQuiz.isEnabled = !isDone
        })
        DispatchQueue.main.async { [weak self] in
            guard let strongSelf = self else{return}
            strongSelf.nameCourses.text = strongSelf.data?.chapter_name
            strongSelf.lbTeacher.text = strongSelf.data?.chapter_teacher
            if strongSelf.data?.chapter_dateStart != nil{
                strongSelf.lbTime.text = String(describing: strongSelf.data?.chapter_dateStart)
            }
            strongSelf.tvDescription.text = strongSelf.data?.chapter_description
        }
        //Add Observer
        NotificationCenter.default.addObserver(self, selector: #selector(rotated), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
        // get idVideo
        idVideo = arrVideo[0].video_id
        numberVideoExist = Video.checkVideoOfflineIsExist(idVideo!) //check video is downloaded
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        customWebView.frame = player.bounds
        customPlayer.frame = player.bounds
    }
    
    //MARK: Handler Add View
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if arrVideo.count > 0 {
            videoType = arrVideo[0].video_type
            if videoType == "HTML"{
                player.addSubview(customWebView)
                customWebView.frame = player.bounds
                customWebView.webView.isOpaque = false
                customWebView.webView.backgroundColor = UIColor.black
                customWebView.webView.delegate = self
                
                if arrVideo.count > 0{
                    let link = arrVideo[0].video_url
                    let reques = URLRequest(url: URL(string:link)!)
                    customWebView.webView.loadRequest(reques)
                }
            } else if videoType == "MP4" {
                player.addSubview(customPlayer)
                customPlayer.frame = player.bounds
                // check if url video offline exit
                if numberVideoExist! > 0 {

                        videoOffline = Video.getVideoOfflineByIdVideo(idVideo!)
                        
                        let link = URL(fileURLWithPath: videoOffline.video_url_offline)
                        customPlayer.player.filepath = String(describing: link)
                        customPlayer.player.lbTitle.text = data?.chapter_name
                        customPlayer.player.startPlayer()
                        btnDownLoad.isEnabled = false
                    
                } else {
                    let link = arrVideo[0].video_url
                    customPlayer.player.filepath = link
                    customPlayer.player.lbTitle.text = data?.chapter_name
                    customPlayer.player.startPlayer()
                    btnDownLoad.isEnabled = true
                }
            }
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        player.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        videoType = arrVideo[0].video_type
        if videoType == "HTML" {
            btnDownLoad.isEnabled = false
        } else {
            btnDownLoad.isEnabled = true
        }
        
        if numberVideoExist! > 0 {
            btnDownLoad.isEnabled = true
        }
        if (self.isMovingFromParentViewController) {
            UIDevice.current.setValue(Int(UIInterfaceOrientation.portrait.rawValue), forKey: "orientation")
        }
    }
    func canRotate() -> Void {}
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        customPlayer.player.releasePlayer()

    }
    
    deinit {
        
    }
    //MARK: Handler orientation
    func rotated()
    {
        if(UIDeviceOrientationIsLandscape(UIDevice.current.orientation))
        {
            print("landscape")
            self.navigationController?.isNavigationBarHidden = true
            AppDelegate.isFullScreen = true
            
        }
        
        if(UIDeviceOrientationIsPortrait(UIDevice.current.orientation))
        {
            print("Portrait")
            self.navigationController?.isNavigationBarHidden = false
            AppDelegate.isFullScreen = false
        }
        
    }
    
    
    @IBAction func btnQuizClicked(_ sender: AnyObject) {
        let quizzView = QuizzViewController()
        
        quizzView.idChapter = self.idChapter
        quizzView.idCourse = self.idCourse
        self.navigationController?.pushViewController(quizzView, animated: true)
    }
   
    //MARK: Handler Download Button
    
    @IBAction func btnDownLoadClicked(_ sender: AnyObject) {
        
        let downLoadVC = DownloadViewController()
        downLoadVC.delegate  = self

        if arrVideo.count > 0{
            if let videoType = String(arrVideo[0].video_type) {
                if videoType == "MP4"{
                    downLoadVC.arrVideo = self.arrVideo
                    downLoadVC.startDownload(urlDownload: arrVideo[0].video_url)
                    
                    // update chapter offline after downloading
                    let myChapter = Chapter.getChaptersByIdChapter(idChapter)
                    _ = Chapter.saveOfflineChapter(myChapter)
                    
                    //
                    let alert = UIAlertController(title: "Message", message: "Downloading....", preferredStyle: .alert)
                    let margin:CGFloat = 8.0
                    let rect = CGRect(x: margin, y: 72.0, width: 250, height: 2.0)
                    progressView = UIProgressView(frame: rect)
                    progressView.setProgress(0.0, animated: false)
                    progressView.tintColor = UIColor.green
                    alert.view.addSubview(progressView)
                    customPlayer.player.playerView.player?.pause()
                    alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (action: UIAlertAction!) in
                        self.customPlayer.player.playerView.player?.play()
                        self.progressView.progress = 0.0
                        
                    }))
                    // post notifi update homeView after save
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "notiUpdateDataOffline"), object: nil)
                    present(alert, animated: true, completion: nil)
                    UIView.animate(withDuration: 0.5, animations: {
                        self.btnDownLoad.isEnabled = false
                        
                        
                    })
                }
            }
        }
    }
    
    //MARK: Handler get proress download
    func getProgress(progress: Float) {
        self.progressDownload =  progress
        DispatchQueue.main.async {
            self.progressView.progress = self.progressDownload!
        }
    }
}

//MARK: webView delegate

extension LessonViewController: UIWebViewDelegate{
    func webViewDidStartLoad(_ webView: UIWebView) {
        customWebView.loading.startAnimating()
    }
    
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        customWebView.loading.stopAnimating()
        customWebView.loading.hidesWhenStopped = true
    }
}

