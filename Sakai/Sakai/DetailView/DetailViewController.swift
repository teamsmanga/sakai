//
//  DetailViewController.swift
//  Sakai
//
//  Created by dang hung on 11/24/16.
//  Copyright © 2016 Team 4. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    @IBOutlet weak var nameCourses: UILabel!
    @IBOutlet weak var imageCourses: UIImageView!
    @IBOutlet weak var btnLesson: UIButton!
    @IBOutlet weak var lbTeacher: UILabel!
    @IBOutlet weak var lbTime: UILabel!
    @IBOutlet weak var tvDescription: UITextView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnLesson.layer.cornerRadius = 5.0
        btnLesson.backgroundColor = orangeSwift
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func btnLessonClicked(_ sender: AnyObject) {

        let lessonVC = self.storyboard?.instantiateViewController(withIdentifier: "lessonViewController") as? LessonViewController
        self.navigationController?.pushViewController(lessonVC!, animated: true)
       
    }

    
}


