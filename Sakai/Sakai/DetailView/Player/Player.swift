//
//  Player.swift
//  PlayerExample
//
//  Created by  Hoan  vu on 10/25/16.
//  Copyright © 2016 abc. All rights reserved.
//

import UIKit
import AVFoundation



protocol PlayerDelegate:class {
    
    
    func playerDidFinish(player:AVPlayer?)
    
    func playerDidFailed()
    
    func playerItemDidChangeRate(player:AVPlayer?)
    
    func playerItemKeepUpTo()
    
    func playerItmeBufferEmpty()
    
    func playerItemReadyToPlay(player:AVPlayer?)
    
    
}




class Player: NSObject {
    
    var player:AVPlayer?
    var playerItem:AVPlayerItem?
    var filePath:String? {
        didSet {
            self.setupAsset()
        }
    }
    
    weak var delegate:PlayerDelegate?

}




//MARK:
extension Player {
    
    func setupAsset() {
        
        if self.filePath != nil {
            let nsurl:NSURL? = NSURL(string: self.filePath!)
            if nsurl != nil {
                let asset:AVURLAsset = AVURLAsset.init(url: nsurl! as URL, options: [:])
                let requestKey = ["playable"]
                asset.loadValuesAsynchronously(forKeys: requestKey, completionHandler:{
                   DispatchQueue.main.async(execute: { [weak self] in
                    guard let strongSelf = self else{return}
                     strongSelf.prepareToPlayAsset(asset: asset)
                   })
                })
            }else {
                
            }
        }
    }
    
    
    
    func prepareToPlayAsset(asset:AVURLAsset) {
        
        if self.playerItem != nil {
            
            self.deallocPlayerItemObserver()
            self.removeNotification()
        }
        
        self.playerItem = AVPlayerItem(asset: asset)
        
        self.registerPlayerItemObserver()
        self.registerNotification()
        
        if(self.player != nil) {
         
            self.player?.removeObserver(self, forKeyPath: "rate")
        }
        
        self.player = AVPlayer(playerItem: self.playerItem!)
        self.player?.seek(to: CMTimeMakeWithSeconds(0, Int32(NSEC_PER_SEC)))
        
        self.player?.addObserver(self,
                                 forKeyPath: "rate",
                                 options:NSKeyValueObservingOptions.new,
                                 context: nil)
        
    }
    
    
   
   
}




//MARK: KVO,NOTIFICATION OF PLAYER

extension Player {
    
    
    
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?)
        {
        
        if keyPath == "status" {
            
            let playerItemStatus:AVPlayerItemStatus = (self.playerItem?.status)!
            
            switch playerItemStatus {
            case .readyToPlay:
                
                self.delegate?.playerItemReadyToPlay(player: self.player)
                player?.play()
                break
                
            case .failed:
                
                self.delegate?.playerDidFailed()
                
                break
                
            case .unknown:
                
                break
            }
            
        } else if keyPath == "rate" {
         
            self.delegate?.playerItemDidChangeRate(player: nil)
            
        }else if keyPath == "playbackBufferEmpty" {
            
            self.delegate?.playerItmeBufferEmpty()
            
        }else if keyPath == "playbackLikelyToKeepUp" {
            
            self.delegate?.playerItemKeepUpTo()
        }
    }
    
    
    
    
    
     func registerPlayerItemObserver() {
        
        
        if self.playerItem != nil {
            
            self.playerItem?.addObserver(self,
                                         forKeyPath: "status",
                                         options: NSKeyValueObservingOptions.new,
                                         context: nil)
            
            self.playerItem?.addObserver(self,
                                         forKeyPath: "playbackBufferEmpty",
                                         options:NSKeyValueObservingOptions.new,
                                         context: nil)
            
            self.playerItem?.addObserver(self,
                                         forKeyPath: "playbackLikelyToKeepUp",
                                         options:  NSKeyValueObservingOptions.new,
                                         context: nil)
            self.registerNotification()
        }
    }
    
    
    
     func deallocPlayerItemObserver() {
        
        if self.playerItem != nil {
            self.playerItem?.removeObserver(self, forKeyPath: "status")
            self.playerItem?.removeObserver(self, forKeyPath: "playbackBufferEmpty")
            self.playerItem?.removeObserver(self, forKeyPath: "playbackLikelyToKeepUp")
            self.removeNotification()
        }
        

        
    }
    
    
    
     func registerNotification() {
        
        NotificationCenter.default.addObserver(self,
                                                         selector: #selector(playerItemDidFinish),
                                                         name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
                                                         object: self.playerItem)
        
        NotificationCenter.default.addObserver(self,
                                                         selector: #selector(playerItemLoadFailure),
                                                         name: NSNotification.Name.AVPlayerItemFailedToPlayToEndTime,
                                                         object: self.playerItem)
    }
    
    
     func removeNotification() {
        
        NotificationCenter.default.removeObserver(self,
                                                            name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
                                                            object: self.playerItem)
        
        NotificationCenter.default.removeObserver(self,
                                                            name: NSNotification.Name.AVPlayerItemFailedToPlayToEndTime,
                                                            object: self.playerItem)

    }
    
    
    func playerItemLoadFailure() {
        
        self.delegate?.playerDidFailed()
    }
    
    
    func playerItemDidFinish() {
        
        self.delegate?.playerDidFinish(player: nil)
        
    }
    
    
    func stopPlayer() {
        
        self.deallocPlayerItemObserver()
        if self.player != nil {
            self.player?.pause()
            self.player?.removeObserver(self, forKeyPath: "rate")
        }
        self.playerItem = nil
        self.player = nil
    }
    
    
    
}


