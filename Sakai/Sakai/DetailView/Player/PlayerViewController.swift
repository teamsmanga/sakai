//
//  PlayerViewController.swift
//  PlayerExample
//
//  Created by  Hoan  vu on 10/25/16.
//  Copyright © 2016 abc. All rights reserved.
//

import UIKit
import AVFoundation


class PlayerViewController: BaseView , PlayerDelegate{

    @IBOutlet weak var playerView: PlayerView!

    
    
    
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var btnPreviours: UIButton!
    @IBOutlet weak var btnFullScreen: UIButton!
    @IBOutlet weak var lbDuration: UILabel!
    @IBOutlet weak var lbTime: UILabel!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var seekbar: UISlider!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    @IBOutlet weak var scubberView: UIView!
    @IBOutlet weak var controlView: UIView!
    

    var m_player:Player?
    var timer:AnyObject?
    var filepath:String?
    var playerViewFullScreen: Bool = false
    
    @IBOutlet var contrainWidth: NSLayoutConstraint!
    
    

    var listCourseItems:[[String:AnyObject]]? = nil
    
    var currentIndex = 0
    var section = 0

    
    
    var currentItem:[String:AnyObject]? = nil {
        didSet {
            
            self.startPlayer()
         
            if let m_title = self.currentItem!["title"] as? String {
                self.lbTitle.text = m_title
            }
        }
    }
    

    


    override func awakeFromNib() {
        self.startPlayer()

        seekbar.minimumValue = 0
        seekbar.isContinuous = true
        seekbar.tintColor = UIColor.green
        seekbar.addTarget(self, action: #selector(playbackSliderValueChanged), for: .valueChanged)
        
        
    }

    deinit {
        print("die")
    }

//MARK: Handler sliderbar
    func playbackSliderValueChanged()  {

        let seekbarValue:Float = self.seekbar.value
        let minValue:Float = Float(self.seekbar.minimumValue)
        let maxValue:Float = self.seekbar.maximumValue
        let position:Double = Double((Float(self.duration) * (seekbarValue - minValue)) / (maxValue - minValue))
        self.m_player?.player?.seek(to: CMTimeMakeWithSeconds(position, Int32(NSEC_PER_SEC)))
        
        self.m_player?.player?.play()
    }


    
    func startPlayer() {
        
    
//        if (self.listCourseItems?.count)! > 0 {
        
            setupPlayer()
            
            m_player = Player()
            m_player?.filePath = filepath
            m_player?.delegate = self
            
//            if self.currentIndex == 0 {
//                self.btnPreviours.isHidden = true
//            }else if self.currentIndex == (self.listCourseItems?.count)! - 1 {
//                self.btnNext.isHidden = true
//            }else {
//                self.btnNext.isHidden = false
//                self.btnPreviours.isHidden = false
//            }
//        }
    }

    func playWithItem(item:[String:AnyObject]) {
        
        if m_player != nil {
            m_player?.stopPlayer()
        }else {
            m_player = Player()
        }
        
        m_player?.filePath = "http://download.prashantmangukiya.com/SwiftVideoPlayer-Data/Big_Buck_Bunny_Trailer.m4v"
        m_player?.delegate = self
        
        
        if let sectionData:[String:AnyObject] = self.listCourseItems![self.section] {
            if let lectures:[[String:AnyObject]] = sectionData["lectures"] as? [[String:AnyObject]] {
                
                if currentIndex == sectionData.count - 1 {
                    if section == lectures.count - 1 {
                        self.btnNext.isHidden = true
                    }else {
                        self.btnNext.isHidden = false
                    }
                }
            }
        }
    }
    
    
    
    func setupPlayer() {
        self.hiddenPlayerControl(flag: true)
        self.hiddenLoadingView(flag: false)
    }
    
    func hiddenPlayerControl(flag:Bool) {
        
        UIView.animate(withDuration: 0.5, animations: {
            if flag == true {
                self.controlView.alpha = 0.5
                self.scubberView.alpha = 0.5
                self.titleView.alpha = 0.5
            }else {
                 self.titleView.alpha = 1.0
                self.controlView.alpha = 1.0
                self.scubberView.alpha = 1.0
            }
        }) { (finish) in
            self.controlView.isHidden = flag
            self.scubberView.isHidden = flag
            self.titleView.isHidden = flag
        }
    }
    
    func hiddenLoadingView(flag:Bool) {
        if flag == false {
            self.hiddenPlayerControl(flag: true)
            loading.startAnimating()
        }
        self.loading.isHidden = flag
    }
}

//MARK: sync scuber
extension PlayerViewController {
    
    
    var duration: Double {
        guard let currentItem = m_player?.player!.currentItem else { return 0.0 }
        return CMTimeGetSeconds(currentItem.duration)
    }
    
    var rate: Float {
        get {
            return (m_player?.player!.rate)!
        }
        set {
            m_player?.player!.rate = newValue
        }
    }
    
    
    var currentTime: Double {
        
       return  CMTimeGetSeconds((self.m_player?.player!.currentTime())!)
    }



    func initScrubberTimer() {
        
        let duration:Double = self.duration
            self.lbDuration.text = self.stringWithtimeInterval(interval: duration)
        
        timer = m_player?.player?.addPeriodicTimeObserver(forInterval: CMTimeMake(1, 100), queue: nil, using: { (time) in
            
            self.syncScubber()
        }) as AnyObject?
    }
    
    
    func syncScubber() {
        
//        DispatchQueue.main.async(execute: {
        
                let minValue:Float = Float(self.seekbar.minimumValue)
                let maxValue:Float = self.seekbar.maximumValue
                
                let delta:Float = (maxValue - minValue) * Float(self.currentTime) / Float(self.duration) + minValue
                let passTime:String = self.stringWithtimeInterval(interval: self.currentTime)
                
                self.lbTime.text = passTime
                self.seekbar.setValue(delta, animated: true)
//        })
        
    }
    
    
    func stringWithtimeInterval(interval:Double) -> String {
        
        let durationDate = NSDate(timeIntervalSince1970: interval)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
        return dateFormatter.string(from: durationDate as Date)
    }
    
    
    func releasePlayer() {
        if timer != nil {
            m_player?.player?.removeTimeObserver(timer!)
            
        }
        if m_player != nil {
            m_player?.stopPlayer()
        }
        m_player = nil
        self.seekbarControl()

    }
    
    func setFullScreenView() {
        if playerViewFullScreen == true {
            self.titleView.isHidden = false
            self.controlView.isHidden = false
        }else {
            self.titleView.isHidden = true
            self.controlView.isHidden = true
        }
    }

    func seekbarControl() {
        
        self.seekbar.value = 0
        self.lbTime.text = "00:00:00"
        self.hiddenLoadingView(flag: true)
        self.m_player?.player?.seek(to: kCMTimeZero)
    }
}


//MARK: ACTION

extension PlayerViewController {
    
    @IBAction func click_fullScreen(sender: AnyObject) {
        if AppDelegate.isFullScreen == false{
            AppDelegate.isFullScreen = true
            let value = UIInterfaceOrientation.landscapeLeft.rawValue
            UIDevice.current.setValue(value, forKey: "orientation")
        }else {
            AppDelegate.isFullScreen = false
            let value = UIInterfaceOrientation.portrait.rawValue
            UIDevice.current.setValue(value, forKey: "orientation")
        }
        self.setFullScreenView()
    }
    
    
    
    @IBAction func click_pause(sender: AnyObject) {
    
        if m_player != nil {
            if self.rate > 0 { // playing
                 self.btnPlay.tag = 1
                self.m_player?.player?.pause()
                self.btnPlay.setImage(UIImage(named: "icon_play"), for: .normal)

               
            }else {
                self.btnPlay.tag = 0
                 self.m_player?.player?.play()
                self.btnPlay.setImage(UIImage(named: "icon_pause"), for: .normal)

            }
        }else {
            self.startPlayer()
            
        }

    }
    
    @IBAction func click_next(sender: AnyObject) {
        
    }
    
    @IBAction func click_previour(sender: AnyObject) {
        
    }
    
    
    @IBAction func tapedPlayerView(sender: AnyObject) {
        if loading.isHidden == true { // loading is hiden
            self.hiddenPlayerControl(flag: !self.controlView.isHidden)
        }
    }
  
   
}

//MARK:PlayerDelegate
extension PlayerViewController {
    
    func playerDidFinish(player:AVPlayer?) {
        
        print("playerDidFinish")

        self.didFinish()
        
    }
    
    func playerDidFailed() {
       
//        self.alertMessage("Message", message: "Play asset error")
    }
    
    func playerItemDidChangeRate(player:AVPlayer?) {
        print("playerItemDidChangeRate")
        
        if rate > 0 {
            self.btnPlay.setImage(UIImage(named: "icon_pause"), for: .normal)
            self.hiddenLoadingView(flag: true)
        }else {
            if self.m_player?.playerItem?.status == AVPlayerItemStatus.readyToPlay {
                if self.btnPlay.tag == 0 {
                    self.hiddenLoadingView(flag: false)
                }else {
                    self.hiddenLoadingView(flag: true)
                }
                self.btnPlay.setImage(UIImage(named: "icon_play"), for: .normal)
            }
        }
    }
    
    func playerItemKeepUpTo() {
      
        if (self.m_player?.playerItem?.isPlaybackBufferEmpty) == false {
            print("playerItemKeepUpTo")
//            btnPlay.setImage(UIImage(named: "icon_pause"), for:UIControlState.normal)
            self.hiddenLoadingView(flag: true)

        }
    }
    
    func playerItmeBufferEmpty() {
        
        if ((self.m_player?.playerItem?.isPlaybackLikelyToKeepUp) == true ) {
            print("playbackLikelyToKeepUp == true")
            btnPlay.setImage(UIImage(named: "icon_play"), for:UIControlState.normal)
            self.hiddenLoadingView(flag: true)
        }else {
            print("playbackLikelyToKeepUp")
            btnPlay.setImage(UIImage(named: "icon_pause"), for:UIControlState.normal)
            self.hiddenLoadingView(flag: false)
        }
    }
    
    func playerItemReadyToPlay(player:AVPlayer?) {
         self.playerView.player = m_player?.player
         self.initScrubberTimer()

    }

    func didFinish() {
        
        self.releasePlayer()
        self.seekbarControl()
        self.hiddenPlayerControl(flag: false)
    }
}

//MARK:CONTROL
extension PlayerViewController {
    
    func next() {
        if self.currentIndex < (self.listCourseItems?.count)! {
            self.currentIndex += 1
            self.currentItem = self.listCourseItems![self.currentIndex]
        }
    }
    
    
    func previour() {
        if self.currentIndex > 0 {
            self.currentIndex -= 1
            self.currentItem = self.listCourseItems![self.currentIndex]
        }
    }
}
