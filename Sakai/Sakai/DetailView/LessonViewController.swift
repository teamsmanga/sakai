//
//  LessonViewController.swift
//  Sakai
//
//  Created by dang hung on 11/24/16.
//  Copyright © 2016 Team 4. All rights reserved.
//

import UIKit

class LessonViewController: UIViewController {
    @IBOutlet weak var playerView: PlayerViewController!
    @IBOutlet weak var tableView: UITableView!
    
    var arrLesson = [String]()
    var lessonVideo = [[String]]()


    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(rotated), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
        let nib = UINib(nibName: "MyTableViewCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "cell")
        
        lessonVideo = [
            ["http://download.prashantmangukiya.com/SwiftVideoPlayer-Data/Big_Buck_Bunny_Trailer.m4v","http://api.mp3.zing.vn/api/mobile/source/video/LGJGTLGNVEJGAJQTVXGTDGLG","http://cdn.liveclips.net/2014/ftb.nfl/56421/0152_079717c7835da1064561bc0b3f47404a1327811f/07971_directv_00_other.mp4","http://api.mp3.zing.vn/api/mobile/source/video/LGJGTLGNQNQDLAVTVXGTDGLG"],
            ["http://download.prashantmangukiya.com/SwiftVideoPlayer-Data/Big_Buck_Bunny_Trailer.m4v","http://api.mp3.zing.vn/api/mobile/source/video/LGJGTLGNQNQNJAJTVXGTDGLG"]
        ]
   
        
        arrLesson = ["section 1","section 2"]
        
        playerView.filepath = lessonVideo[0][0]
        playerView.startPlayer()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        playerView.releasePlayer()
    }
    
    deinit {
       
    }
    //MARK: Handler orientation
        func rotated()
        {
            if(UIDeviceOrientationIsLandscape(UIDevice.current.orientation))
            {
                print("landscape")
                self.navigationController?.isNavigationBarHidden = true
                AppDelegate.isFullScreen = true
    
            }
    
            if(UIDeviceOrientationIsPortrait(UIDevice.current.orientation))
            {
                print("Portrait")
                    self.navigationController?.isNavigationBarHidden = false
                    AppDelegate.isFullScreen = false


            }
            
            
        }
    

}

extension LessonViewController : UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrLesson.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.lessonVideo [section].count
    }
     func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        return self.arrLesson [section]
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MyTableViewCell
       cell.lbName.text = lessonVideo[indexPath.section][indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let link = lessonVideo[indexPath.section][indexPath.row]
        if playerView.m_player?.player?.rate == 1{
            playerView.m_player?.player?.pause()
            playerView.releasePlayer()
            playerView.filepath = link
            playerView.startPlayer()
        }
      
        
    }
}
