//
//  LocalNotification.swift
//  Sakai
//
//  Created by PhatLe on 11/25/16.
//  Copyright © 2016 Team 4. All rights reserved.
//

import Foundation
import UserNotifications

class LocalNotification: NSObject{

    private override init(){}
    
    static var shareInstance = LocalNotification()
    func pushnotiwhendownloadcompleted(title:String, body:String){
        let content = UNMutableNotificationContent()
        let requestIdentifier = "rajanNotification"
        content.badge = 1
        content.title = title
        content.body = body
        content.categoryIdentifier = "actionCategory"
        content.sound = UNNotificationSound.default()
        let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 1.0, repeats: false)
        let request = UNNotificationRequest(identifier: requestIdentifier, content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request) { (error:Error?) in
            if error != nil {
                print((error?.localizedDescription)!)
            }     
            print("Notification Register Success")
        }
    }
    
    func registerForNotifications(){
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert,.badge,.sound]) { (granted:Bool, error:Error?) in
            if error != nil {
                print((error?.localizedDescription)!)
            }
            if granted {
                print("Permission granted")
                
            } else {
                print("Permission not granted")
            }
        }
        let action = UNNotificationAction(identifier: "action", title: "Download", options: [.foreground])
        let category = UNNotificationCategory(identifier: "actionCategory", actions: [action], intentIdentifiers: [], options: [])        
        UNUserNotificationCenter.current().setNotificationCategories([category])

    }
}

extension LocalNotification: UNUserNotificationCenterDelegate{
//    for displaying notification when app is in foreground


    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        //If you don't want to show notification when app is open, do something here else and make a return here.
        //Even you you don't implement this delegate method, you will not see the notification on the specified controller. So, you have to implement this delegate and make sure the below line execute. i.e. completionHandler.
        
        completionHandler([.alert,.badge])
    }
}
